"""
Created on 2 Aug 2012

@author: julian
"""

import logging


# default timeout for the node's response
DEFAULT_TIMEOUT = 40


class Node(object):
    """
    Basic node.

    Provides network connection and sensor sample period manipulation.
    """

    _addr = None
    _conn = None
    _sample_period = None

    def __init__(self, addr, conn):
        """
        Args:
            addr: EUI-64, 64bit MAC address of the node
            conn = Conn object from connection.py
        """
        super(Node, self).__init__()
        self._addr = addr
        self._conn = conn

    def get_address(self):
        """
        Get the EUI-64 address of the node.

        Returns:
            EUI-64 of the node as string
        """
        return self._addr

    def set_address(self, addr):
        """
        Set the EUI-64 address of the node
        Args:
            addr: EUI-64 of the node as string
        """
        self._addr = str(addr)

    def get_connection(self):
        """
        Get the Conn object used to communicate with the Arch Rock Server.

        Returns:
            Conn object being used to connect to the server
        """
        return self._conn

    def set_connection(self, conn):
        """
        Set the Conn object to be used to communicate with the Arch Rock Server.

        Args:
            conn: Conn object to be used to connect to the server
        """
        self._conn = conn

    def get_sample_period(self, timeout=DEFAULT_TIMEOUT):
        """
        Get the sample period.

        Sample period defines how often the node will send its readings
        (the Event message) to the Arch Rock Server.

        This method communicate with the node directly, therefore can be slow.

        Args:
            timeout: Time to wait for the node's response.

        Returns:
            Sample period in miliseconds. Returns None if the sample period
            is not known.
        """
        param = ("method=attributes.get&name=SamplePeriod&addr="
                 + self._addr + "&timeout=" + str(timeout))
        dom = self._conn.REST_GET(param)
        return self._parse_sample_period_response(dom)

    def set_sample_period(self, period, timeout=DEFAULT_TIMEOUT):
        """
        Set the sample period.

        Sample period defines how often the node will send its readings
        (the Event message) to the Arch Rock Server.

        This method communicates with the node directly, so it can be slow.

        Args:
            period: Sample period for the node (msec)
            timeout: Time to wait for the node's response
        """
        recv_period = self._send_sample_period(period, timeout)
        if recv_period != period:
            logging.warning("Sample period returned by the node "
                            + "does not equal the value set by the user")

    def _send_sample_period(self, period, timeout):
        """
        Send a REST GET (yes, GET) to set a new sample period

        Args:
            period: New sample period (msec)
            timeout: Time to wait for the node response
        Returns:
            Response from the node (dom object)
        """
        param = ("method=attributes.set&name=SamplePeriod&addr="
                 + self._addr + "&timeout=" + str(timeout)
                 + "&value=" + str(period))
        dom = self._conn.REST_GET(param)
        return self._parse_sample_period_response(dom)

    def _parse_sample_period_response(self, dom):
        """
        Extract the sample period from a dom object.

        Args:
            dom: dom object containing the sample period
        Returns:
            Integer value of sample period (msec), or None if a problem occured
        """
        result_tag = dom.getElementsByTagName("Result")
        value_tag = result_tag[0].getElementsByTagName("Value")
        if len(value_tag) == 0:
            return None
        return int(value_tag[0].firstChild.toxml())

    def blink(self):
        """
        Blink a LED on the node.
        """
        param = ("method=rpc.execute&name=heartbeatRequest&addr="
                 + self._addr)
        return self._conn.REST_GET(param)

    def ping(self):
        """
        Test the connection to the node. Also verifies that routing has been
        agreed.
        """
        dom = self._send_ping()
        return self._parse_ping_response(dom)

    def _send_ping(self):
        """
        Send a ping request.

        Returns:
            A dom object containing the ping response
        """
        param = ("method=mgmt.pingNode&name=heartbeatRequest&addr="
                 + self._addr)
        return self._conn.REST_GET(param)

    def _parse_ping_response(self, dom):
        """
        Parse the ping response to see whether it contains the right EUI-64.

        Args:
            dom: A dom object containing the ping response.

        Returns:
            True if the response contains the right address. False otherwise.
        """
        addr_tag = dom.getElementsByTagName("longAddr")
        if len(addr_tag) == 0:
            return False

        if addr_tag[0].firstChild.toxml() == self.get_address():
            return True

        return False

    def check_data_errors(self, dom):
        """
        Parse a reponse from the node to see if there are any errors.

        Args:
            dom: dom object containing the response
        Returns:
            Nothing, but will make a mess in stdout

        TODO: this smells like a disaster, change it
        """
        err_tag = dom.getElementsByTagName('Error')
        for tag in err_tag:
            logging.warning("at node " + self.get_address() + (
                    ' ' + tag.getAttributeNode('code').nodeValue))

class HTChannels():
    """
    HTChannels manage the temperature and humidity
    reported by the node's internal sensors.
    """
    _temp = None
    _hum = None

    _temp_enabled = None
    _hum_enabled = None

    def __init__(self):
        pass

    def get_internal_temperature(self):
        """
        Get the internal temperature reading.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Return:
            Temperature (deciCelsius) as reported in the last Event message.
        """
        return self._temp

    def _set_internal_temperature(self, temp):
        """
        Set the internal tempareture
        Args:
            temp: New temperature value (deciCelsius)
        """
        self._temp = temp

    def get_internal_humidity(self):
        """
        Get the internal humidity value.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            The humidity (deci %) as reported in the last Event message.
        """
        return self._hum

    def _set_internal_humidity(self, hum):
        """
        Set internal humidity.

        Args:
            hum: New humidity value (deci %)
        """
        self._hum = hum

    def is_internal_temperature_enabled(self):
        """
        Check whether the internal temperature sensor is enabled.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            True if the channels is configured to report the temperature,
            False if it is not,
            None if it is not known to this class.
        """
        return self._temp_enabled

    def enable_internal_temperature(self, boolean):
        """
        Enable / disable the internal temperature sensor.

        This method does not communicate with the node directly.
        Run separate methods to send this value to the node.

        Args:
            boolean: True to enable the temperature reporting, False to disable.
        """
        if type(boolean) == bool:
            self._temp_enabled = boolean

    def is_internal_humidity_enabled(self):
        """
        Check whether the internal humidity sensor is enabled.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            True if the channels is configured to report the humidity,
            False if it is not,
            None if it is not known to this class.
        """
        return self._hum_enabled

    def enable_internal_humidity(self, boolean):
        """
        Enable / disable internal humidity sensor.

        This method does not communicate with the node directly.
        Run separate methods to send this value to the node.

        Args:
            boolean: True to enable the humidity reading, False to disable
        """
        if type(boolean) == bool:
            self._hum_enabled = boolean

    def update_HTChannels(self, dom):
        """
        Use the node's response to update the values of temperature
        and humidity.

        Args:
            dom: Event object obtained from the node.

        Warning: Arch Rock doc is unclear about
        the faulty states reported by the htChannels.
        The state is usually 0 or 2, and it behaves correctly.
        Otherwise you are on your own
        """
        ht_channels = dom.getElementsByTagName("htChannels")
        if len(ht_channels) == 0:
            logging.warning("No values in htChannels. Possible timeout")
            return

        internal_sens = ht_channels[0]

        state_tag = internal_sens.getElementsByTagName("state")
        state = int(state_tag[0].firstChild.toxml())

        if state == 0:
            self._set_internal_temperature(None)
            self._set_internal_humidity(None)
            return

        temp_tag = internal_sens.getElementsByTagName("temp")
        temp = int(temp_tag[0].firstChild.toxml())

        hum_tag = internal_sens.getElementsByTagName("humidity")
        hum = int(hum_tag[0].firstChild.toxml())

        if state == 2: # state 'correct values'
            self._set_internal_temperature(temp)
            self._set_internal_humidity(hum)
        else: # something is wrong
            self._set_internal_temperature(None)
            self._set_internal_humidity(None)

    def update_HTconfig(self, dom):
        """
        Use the Config response from the node to update the state.

        Config will be used to enable / disable the temperature and
        humidity sensors.
        Drops previous data so in case of a timeout, old state will be deleted.

        Args:
            dom: Config dom file obtained from the node
        """
        self._temp_enabled = None
        self._hum_enabled = None
        ht_channels = dom.getElementsByTagName("htChannels")
        if len(ht_channels) == 0:
            return

        internal_state = ht_channels[0]

        temp_tag = internal_state.getElementsByTagName("tempEnabled")
        temp = int(temp_tag[0].firstChild.toxml())

        hum_tag = internal_state.getElementsByTagName("humidEnabled")
        hum = int(hum_tag[0].firstChild.toxml())

        if temp == 1:
            self.enable_internal_temperature(True)
        else:
            self.enable_internal_temperature(False)

        if hum == 1:
            self.enable_internal_humidity(True)
        else:
            self.enable_internal_humidity(False)

    def get_HTconfig_string(self):
        """
        Construct a string containing the configuration of the HT Channels.

        Returns:
            The configuration string.
        """
        if ((self.is_internal_temperature_enabled() == None)
                or (self.is_internal_humidity_enabled() == None)):
            logging.warning("HTconfig string cannot be created")
            return

        temp = "1"
        if self._temp_enabled == False:
            temp = "0"

        hum = "1"
        if self._hum_enabled == False:
            hum = "0"

        return ("&value.htChannels.element0.tempEnabled=" + temp
                + "&value.htChannels.element0.humidEnabled=" + hum
                + "&value.htChannels.element0.tempUpper=0"
                + "&value.htChannels.element0.tempLower=0"
                + "&value.htChannels.element0.humidUpper=0"
                + "&value.htChannels.element0.humidLower=0")


class XTChannels():
    """
    XT Channel manages the temperatures recorded by the external sensors
    There are 6 temperatures (A1 at 1, A2 at 2, B1 at 3 etc)
    """
    _temps = []
    _temps_enabled = []
    _temps_R0 = []
    _temps_B = []

    def __init__(self):
        pass

    def get_temperatures(self):
        """
        Get the temperature readings.

        This method does not communicate with the node directly.
        Run separate methods to update these values.

        Returns:
            A list of 6 temperatues (in deciCelsius),
            or an empty list if the temperatuers are not available
        """
        return self._temps

    def _set_temperatures(self, temps):
        """
        Set the temperature readings.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            temps: A list of 6 temperatures (in deciCelsius). If the list
                contains a different number of elements, an empty list will
                be set.
        """
        if len(temps) == 6:
            self._temps = temps
        else:
            logging.warning("too many temperatures")
            self._temps = []

    def are_temperatures_enabled(self):
        """
        Check whether the temperature sensors are enabled.

        Returns:
            A list of 6 boolean values showing whether reading is
            enabled or not.
            Returns an empty list if the states are not available.
        """
        return self._temps_enabled

    def enable_temperatures(self, bool_arr):
        """
        Enable / disable the temperature sensors.

        Args:
            bool_arr: A list of 6 boolean values showing whether a reading
            should be enabled or not. If the list does not contain
            6 bools, then an empty list will be set.
        """
        if len(bool_arr) == 0:
            self._temps_enabled = []
            return
        else:
            for ba in bool_arr:
                if type(ba) != bool:
                    self._temps_enabled = []
                    return
            self._temps_enabled = bool_arr

    def get_temperatures_R0(self):
        """
        Returns:
            A list of 6 R0 values for the temperature probes.
        """
        return self._temps_R0

    def set_temperatures_R0(self, arr):
        """
        Args:
            arr: A list of 6 R0 values for the temperature probes.
        """
        if len(arr) != 6:
            self._temps_R0 = []
            return

        self._temps_R0 = arr

    def get_temperatures_B(self):
        """
        Returns:
            A list of 6 B values for the temperature probes.
        """
        return self._temps_B

    def set_temperatures_B(self, arr):
        """
        Args:
            arr: A list of 6 B values for the temperature probes.
        """
        if len(arr) != 6:
            self._temps_B = []
            return

        self._temps_B = arr

    def update_XTChannels(self, dom):
        """
        Update the temperature readings using a provided dom object

        Args:
            dom: A dom object containing the Event received by the node.
        """
        self._temps = []
        rt_channels = dom.getElementsByTagName("xtChannels")
        if len(rt_channels) == 0:
            logging.warning("No values in xtChannels. Possible timeout")
            return
        external_temp = rt_channels[0]
        elements = external_temp.getElementsByTagName("Element")

        for element in elements:
            state_tag = element.getElementsByTagName("state")
            state = int(state_tag[0].firstChild.toxml())

            if state == 1:
                temp_tag = element.getElementsByTagName("temp")
                temp = int(temp_tag[0].firstChild.toxml())
                if temp == -32767:
                    self._temps.append(None)
                else:
                    self._temps.append(temp)
            else:
                self._temps.append(None)

    def update_XTconfig(self, dom):
        """
        Update the configuration using the Config message from the node.

        See whether probes are enabled and read their R0 and B values.
        Drops previous data so in case of a timeout, old state will be deleted.

        Args:
            dom: A dom object containing the Config received by the node.
        """
        self._temps_enabled = []
        self._temps_R0 = []
        self._temps_B = []

        xt_channels = dom.getElementsByTagName("xtChannels")
        if len(xt_channels) == 0:
            return

        elements = xt_channels[0].getElementsByTagName("Element")
        for element in elements:

            enabled_tag = element.getElementsByTagName("enabled")
            enabled = int(enabled_tag[0].firstChild.toxml())
            if enabled == 1:
                self._temps_enabled.append(True)
            else:
                self._temps_enabled.append(False)

            R0_tag = element.getElementsByTagName("R0")
            R0 = float(R0_tag[0].firstChild.toxml())
            self._temps_R0.append(R0)

            B_tag = element.getElementsByTagName("B")
            B = float(B_tag[0].firstChild.toxml())
            self._temps_B.append(B)

    def get_XTconfig_string(self):
        """
        Construct a string containing the configuration of the XT Channels

        Returns:
            A string to be sent to the server to configure the node.
        """
        if ((self._temps_enabled == [])
            or (self._temps_R0 == [])
            or (self._temps_B == [])):
            logging.warning("XTconfig string cannot be created")
            return

        enabled = []
        for b in self.are_temperatures_enabled():
            if b == True:
                enabled.append("1")
            else:
                enabled.append("0")

        conf_string = ""
        for i in range(6):
            conf_string = (conf_string
                + "&value.xtChannels.element" + str(i)
                    + ".enabled=" + enabled[i]
                + "&value.xtChannels.element" + str(i)
                    + ".R0=" + str(self._temps_R0[i])
                + "&value.xtChannels.element" + str(i)
                    + ".B=" + str(self._temps_B[i]))

        return conf_string


class DPChannels():
    """
    Manage the data from the 3 differential pressure sensors.
    """
    _pressures = []
    _pressures_enabled = []
    _pressures_ambient = []

    def __init__(self):
        pass

    def get_pressures(self):
        """
        Get the pressure readings stored by the object.

        This method does not trigger the communication with the node.
        Use separate methods to refresh these values.

        Returns:
            A list of 3 differential pressure readings (micro Pa).
            Returns an empty list if the readings are not available.
        """
        return self._pressures

    def _set_pressures(self, press):
        """
        Set the pressure values.

        This method does not trigger the communication with the node.
        Use separate methods to refresh these values.

        Args:
            press: A list of 3 pressures values (micro Pa). If a different
                umber of elements is provided, an empty list will be set.
        """
        if len(press) == 3:
            self._pressures = press
        else:
            self._pressures = []
            logging.warning("!! ERROR wrong number of pressures")

    def are_pressures_enabled(self):
        """
        Check if the sensors are enabled.

        This method does not trigger the communication with the node.
        Use separate methods to update these values.

        Returns:
            A list of 3 boolean values showing whether reading is enabled of not.
            Returns an empty list if the values are not available.
        """
        return self._pressures_enabled

    def enable_pressures(self, bool_arr):
        """
        Enable pressure sensors.

        This method does not trigger the communication with the node.
        Use separate methods to update these values.

        Args:
            bool_arr: A list of 3 bool values showing whether to enable the
            readings or not. An empty list will be set if bool_arr does not
            contain 3 bool values
        """
        if len(bool_arr) != 3:
            self._pressures_enabled = []
            return
        for ba in bool_arr:
            if type(ba) != bool:
                return
        self._pressures_enabled = bool_arr

    def get_ambient_pressures(self):
        """
        Get the ambient pressures of the environment.

        This method does not trigger the communication with the node.
        Use separate methods to update these values.

        Returns:
            A list containing 3 ambient pressure values for the sensors.
            Returns an empty list if these values are not known.
        """
        return self._pressures_ambient

    def set_ambient_pressures(self, int_arr):
        """
        Set the ambient pressures of the environment.

        Args:
            int_arr: A list of 3 ambient pressure values used to configure
            the pressures sensors. An empty list will be used if
            the int_arr does not contain 3 bools.
        """
        if len(int_arr) != 3:
            return
        for ia in int_arr:
            if type(ia) != int:
                return
        self._pressures_ambient = int_arr

    def update_DPChannels(self, dom):
        """
        Parse an Event response and update the pressure readings.

        This method does not trigger the communication with the node.
        Drops previous data, so in case of a timeout, the old readings
        will be lost.

        Args:
            dom: A dom object containing the Event response from the node.
        """
        self._pressures = []

        dp_channels = dom.getElementsByTagName("dpChannels")
        pressures = dp_channels[0]
        elements = pressures.getElementsByTagName("Element")

        for element in elements:
            state_tag = element.getElementsByTagName("state")
            state = int(state_tag[0].firstChild.toxml())

            if state == 1:
                press_tag = element.getElementsByTagName("mdp")
                press = int(press_tag[0].firstChild.toxml())
                #if press == -32767:
                #    self._pressures.append(None)
                #else:
                self._pressures.append(press)
            else:
                self._pressures.append(None)

    def update_DPconfig(self, dom):
        """
        Update the internal state using the Config response from the node.

        Read the Config received by the node, enable/disable readings and
        update the ambient pressures for each pressure probe.
        Drops previous data so in case of a timeout, old state will be deleted.

        Args:
            dom: A dom object containing the Config received from the node.
        """
        self._pressures_enabled = []
        self._pressures_ambient = []

        dp_channels = dom.getElementsByTagName("dpChannels")
        if len(dp_channels) == 0:
            return

        elements = dp_channels[0].getElementsByTagName("Element")
        for element in elements:

            enabled_tag = element.getElementsByTagName("enabled")
            enabled = int(enabled_tag[0].firstChild.toxml())
            if enabled == 1:
                self._pressures_enabled.append(True)
            else:
                self._pressures_enabled.append(False)

            pamb_tag = element.getElementsByTagName("pamb")
            pamb = int(pamb_tag[0].firstChild.toxml())
            self._pressures_ambient.append(pamb)

    def get_DPconfig_string(self):
        """
        Create a string that can be used to send the configuration to the node.

        Returns:
            A string with the configuration of DP channels.
        """
        enabled = []
        for e in self.are_pressures_enabled():
            if e == True:
                enabled.append("1")
            else:
                enabled.append("0")

        conf_string = ""
        for i in range(3):
            conf_string = (conf_string
                + "&value.dpChannels.element" + str(i)
                    + ".enabled=" + enabled[i]
                + "&value.dpChannels.element" + str(i)
                    + ".pamb=" + str(self._pressures_ambient[i]))
        return conf_string

class IPThermal_RSN3040_6XT(Node, HTChannels, XTChannels):
    """
    Arch Rock XT Temperature node
    RSN-3040-6XT

    Consists of HT Channels (internal temperature and humidity)
    and XT Channels (6 external temperatures)
    """

    def __init__(self, addr, conn):
        super(IPThermal_RSN3040_6XT, self).__init__(addr, conn)

    def read_sensors(self, timeout):
        """
        Send a REST GET to the server to get the last reading reported by
        the node.

        The node is not contacted. It has a benefit over the
        'read_sensors_expensive' as it does not pollute network, and also
        it helps to reduce battery drain. Nodes report the readings as defined
        by the sample period value.

        Args:
            timeout: Time to wait for the node response (sec)

        Returns:
            A dom object containg the Event response.
        """
        param = ("method=events.readLast&name=RSN30406XTReadEvent&addr="
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def read_sensors_expensive(self, timeout):
        """
        Send a REST GET to the node to get the current state observed by
        the node.

        It communicates directly with the node, polluting the network
        and reducing battery life. Also is more prone to unexpected results,
        such as timeouts. Gets more up-to-data ignoring the sample period.

        Args:
            timeout: Time to wait for the node response (sec)

        Returns:
            A dom object containg the Event response.
        """
        param = ("method=attributes.get&name=RSN30406XTReadEvent&addr="
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def read_config(self, timeout):
        """
        Send a REST GET to the node to obtain the Config response.

        Args:
             timeout: Time to wait for the node response (sec)

        Returns:
            A dom object containg the Config response.
        """
        param = ("method=attributes.get&name=RSN30406XTConfig&addr="
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def update_data(self, timeout=DEFAULT_TIMEOUT):
        """
        Ask the server for the last reported sensors' data
        and update the internal readings.

        Has an advantage over 'update_data_expensive' as it does not contact
        the node itself, reducing the network traffic and increasing
        battery life. The data at the server is updated accordingly to the
        sample period of the node.

        Args:
             timeout: Time to wait for the node response (sec)

        Returns:
            A dom object containing the readings
        """
        dom = self.read_sensors(timeout)
        self.update_HTChannels(dom)
        self.update_XTChannels(dom)
        self.check_data_errors(dom)

        return dom

    def update_data_expensive(self, timeout=DEFAULT_TIMEOUT):
        """
        Ask the node for the current sensors' data
        and update the internal readings.

        Contacts the node directly, polluing the network and reducing
        battery life. On the other hand gets as up-to-date data as possible,
        ignoring the delay caused by low sample period.

        Args:
             timeout: Time to wait for the node response (sec)

        Returns:
            A dom object containing the readings.
        """
        dom = self.read_sensors_expensive(timeout)
        self.update_HTChannels(dom)
        self.update_XTChannels(dom)
        self.check_data_errors(dom)

        return dom

    def update_config(self, timeout=DEFAULT_TIMEOUT):
        """
        Ask the node for the current configuration
        and update the internal state.

        Args:
             timeout: Time to wait for the node response (sec)
        """
        dom = self.read_config(timeout)
        self.update_HTconfig(dom)
        self.update_XTconfig(dom)

    def send_config(self, timeout=DEFAULT_TIMEOUT):
        conf_string = ("method=attributes.set&name=RSN30406XTConfig"
                       + "&addr=" + self.get_address()
                       + "&timeout=" + str(timeout)
                       + self.get_HTconfig_string()
                       + self.get_XTconfig_string())

        dom = self._conn.REST_GET(conf_string)
        return dom


class IPThermal_RSN3040_3DP(Node, HTChannels, DPChannels):
    """
    Differential pressure node
    RSN-3040-3DP

    Records:
        internal temperature
        internal humidity
        3 differential pressures
    """

    def __init__(self, addr, conn):
        super(IPThermal_RSN3040_3DP, self).__init__(addr, conn)

    def read_sensors(self, timeout):
        """
        Send a REST GET to the server to get the last reading reported by
        the node.
        The node is not contacted. It has a benefit over the
        'read_sensors_expensive' as it does not pollute network, and also
        it helps to reduce battery drain. Nodes report the readings as defined
        by the sample period value.

        Args:
            timeout: Time to wait for the node response (sec)

        Returns:
            A dom object containg the Event response.
        """
        param = ('method=events.readLast&name=RSN30403DPReadEvent&addr='
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def read_sensors_expensive(self, timeout):
        """
        Send a REST GET to the node to obtain the current state.

        Contacts the node directly, polluting the network and reducing battery
        life. On the other hand gets as up-to-date data as possible.

        Args:
            timeout: Time to wait for the node response (sec)

        Returns:
            A dom object containg the Event response.
        """
        param = ('method=attributes.get&name=RSN30403DPReadEvent&addr='
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def update_data(self, timeout=DEFAULT_TIMEOUT):
        """
        Ask the server for the last reported sensors' data
        and update the internal readings.

        It has a benefit over the 'update_data_expensive' as it does not
        contact the node directly, thus reducing network traffic and increasing
        battery life. The data on the server is updated as often as defined
        in the node's sample period.

        Args:
             timeout: Time to wait for the node response (sec)

        Returns:
            A dom object containing the readings
        """
        dom = self.read_sensors(timeout)
        self.update_HTChannels(dom)
        self.update_DPChannels(dom)
        self.check_data_errors(dom)

        return dom

    def update_data_expensive(self, timeout=DEFAULT_TIMEOUT):
        """
        Ask the node for the current sensors' data
        and update the internal readings.

        It contacts the node directly, polluting the network and reducing
        battery life. On the other hand it gets as up-to-date data as possible,
        without waiting for the update triggered by the node's sample period.

        Args:
             timeout: Time to wait for the node response (sec)

        Returns:
            A dom object containing the readings
        """
        dom = self.read_sensors_expensive(timeout)
        self.update_HTChannels(dom)
        self.update_DPChannels(dom)
        self.check_data_errors(dom)

        return dom

    def update_config(self, timeout=DEFAULT_TIMEOUT):
        """
        Ask the node for the current configuration
        and update the internal state.

        Args:
             timeout: Time to wait for the node response (sec)
        """
        dom = self.read_config(timeout)
        self.update_HTconfig(dom)
        self.update_DPconfig(dom)

    def read_config(self, timeout):
        """
        Send a REST GET to the node to obtain the Config response.

        Args:
             timeout: Time to wait for the node response (sec)

        Returns:
            A dom object containg the Config response.
        """
        param = ("method=attributes.get&name=RSN30403DPConfig&addr="
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)


    def send_config(self, timeout=DEFAULT_TIMEOUT):
        conf_string = ("method=attributes.set&name=RSN30403DPConfig"
                       + "&addr=" + self.get_address()
                       + "&timeout=" + str(timeout)
                       + self.get_HTconfig_string()
                       + self.get_DPconfig_string())

        dom = self._conn.REST_GET(conf_string)
        self.update_HTconfig(dom)
        self.update_DPconfig(dom)
        return dom


class IPSensor_RSN3010_2ADC(Node):
    """
    Manages an IPSensor node with 4 I/O ports and 2 ADCs.
    """

    _hum = None
    _temp = None
    _light_tsr = None
    _light_par = None
    _switch = None
    _v0 = None

    _hum_enabled = None
    _temp_enabled = None

    _light_tsr_enabled = None
    _light_par_enabled = None

    _ports = ("Switch", "IO0", "IO1", "IO2")

    _switch_enabled = None
    _switch_sampling = None
    _switch_interrupting = None

    _IO0 = None
    _IO0_enabled = None
    _IO0_sampling = None
    _IO0_as_output = None
    _IO0_interrupting = None

    _IO1 = None
    _IO1_enabled = None
    _IO1_sampling = None
    _IO1_as_output = None
    _IO1_interrupting = False

    _IO2 = None
    _IO2_enabled = None
    _IO2_sampling = None
    _IO2_as_output = None
    _IO2_interrupting = False

    def __init__(self, addr, conn):
        super(IPSensor_RSN3010_2ADC, self).__init__(addr, conn)

    def is_internal_temperature_enabled(self):
        """
        Check whether the internal temperature sensor is enabled.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            True when the sensor is enabled,
            False if not,
            None if the state is not known
        """
        return self._temp_enabled

    def get_internal_temperature(self):
        """
        Get internal temperature value.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            Integer temperature as deciCelsius
        """
        return self._temp

    def _request_temperature_event(self, timeout=DEFAULT_TIMEOUT):
        """
        Send a REST GET to the server to get the last reading reported by
        the node.

        The node is not contacted. It has a benefit over the
        'read_sensors_expensive' as it does not pollute network, and also
        it helps to reduce battery drain. Nodes report the readings as defined
        by the sample period value.

        Args:
            timeout: Time to wait for the node response

        Returns:
            A dom object with the temperature as miliFarenheit
        """
        param = ("method=events.readLast&name=TemperatureReadEvent&addr="
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def _request_temperature_event_expensive(self, timeout=DEFAULT_TIMEOUT):
        """
        Send a REST GET request to the node to get
        the current temperature from the sensor.

        It contacts the node directly, polluting the network and reducing
        battery life. On the other hand it gets as up-to-date data as possible
        without waiting for the update triggered by the sample period.

        Args:
            timeout: Time to wait for the node response

        Returns:
            A dom object with the temperature as miliFarenheit
        """
        param = ("method=attributes.get&name=TemperatureReadEvent&addr="
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def _parse_temperature_event(self, dom):
        """
        Parse a dom object contatining the temperature recorded by the sensor.

        Args:
            dom: The object containing the temperature as miliFarenheit

        Returns:
            Temperature as a rounded integer deciCelsius
        """
        value_tag = dom.getElementsByTagName("Value")
        if len(value_tag) == 0:
            return None

        milifarenheit = int(value_tag[0].firstChild.toxml())
        milicelsius = (milifarenheit - 3200) * 5 / 9
        decicelsius = milicelsius / 10.0
        return int(round(decicelsius))

    def update_internal_temperature(self, timeout=DEFAULT_TIMEOUT):
        """
        Get the last temperature reading reported by the sensor
        and store the value.

        It does not contact the node directly, reducing
        the network traffic and increasing battery life. The data update is
        triggered by the node's sample period.

        Args:
            timeout: Time to wait for the node response.
        """
        dom_temp = self._request_temperature_event(timeout)
        self._temp = self._parse_temperature_event(dom_temp)

    def update_internal_temperature_expensive(self, timeout=DEFAULT_TIMEOUT):
        """
        Get current temperature from the sensor and store the value.

        It contacts the node directly, polluting the network and reducing
        battery life. On the other hand it gets as up-to-date data as possible
        without waiting for the update triggered by the sample period.

        Args:
            timeout: Time to wait for the node response.
        """
        dom_temp = self._request_temperature_event_expensive(timeout)
        self._temp = self._parse_temperature_event(dom_temp)

    def is_internal_humidity_enabled(self):
        """
        Check whether the intenal humidity sensor is enabled.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            True when the sensor is enabled,
            False if not,
            None if the state is not known
        """
        return self._hum_enabled

    def get_internal_humidity(self):
        """
        Get internal humidity value.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            Integer humidity as decipercent
        """
        return self._hum

    def _request_humidity_event(self, timeout=DEFAULT_TIMEOUT):
        """
        Send a REST GET request to the server to get the last humidity
        value reported by the sensor.

        It does not contact the node directly,
        reducing network traffic and increasing battery life. The data at the
        server is updated accordingly to the node's sample period.

        Args:
            timeout: Time to wait for the node response

        Returns:
            A dom object with the humidity as milipercent
        """
        param = ("method=events.readLast&name=HumidityReadEvent&addr="
                 + self._addr + "&timeout=" + str(timeout))

        return self._conn.REST_GET(param)

    def _request_humidity_event_expensive(self, timeout=DEFAULT_TIMEOUT):
        """
        Send a REST GET request to the node to get the current humidity
        from the sensor.

        It contacts the node directly, polluting the network
        and reducing battery life. On the other hand it gets as up-to-date data
        as possible, without waiting for the update triggered by the
        sample period.

        Args:
            timeout: Time to wait for the node response

        Returns:
            A dom object with the humidity as milipercent
        """
        param = ("method=attributes.get&name=HumidityReadEvent&addr="
                 + self._addr + "&timeout=" + str(timeout))

        return self._conn.REST_GET(param)

    def _parse_humidity_event(self, dom):
        """
        Parse a dom object contatining the humidity recorded by the sensor.

        Args:
            dom: The object containing the humidity as miliPercent

        Returns:
            Humidity as a rounded integer deciPercent
        """
        value_tag = dom.getElementsByTagName("Value")
        if len(value_tag) == 0:
            return None

        milipercent = int(value_tag[0].firstChild.toxml())
        decipercent = int(round(milipercent / 10.0))
        return decipercent

    def update_internal_humidity(self, timeout=DEFAULT_TIMEOUT):
        """
        Get the last humidity value reported by the sensor to the server.

        It does not contact the node directly, reducing the network traffic
        and increasing battery life. The data as the server is updated as
        defined by the node's sample period.

        Args:
            timeout: Time to wait for the node response.
        """
        dom_hum = self._request_humidity_event(timeout)
        self._hum = self._parse_humidity_event(dom_hum)

    def update_internal_humidity_expensive(self, timeout=DEFAULT_TIMEOUT):
        """
        Get current humidity from the sensor.

        It contacts the node directly, polluting the network and reducing
        battery life. On the other hand it gets as up-to-date data as possible
        without waiting for the update to be triggered by the sample period.

        Args:
            timeout: Time to wait for the node response.
        """
        dom_hum = self._request_humidity_event_expensive(timeout)
        self._hum = self._parse_humidity_event(dom_hum)

    def is_light_tsr_enabled(self):
        """
        Check whether the light TSR sensor is enabled.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            True when the sensor is enabled,
            False if not,
            None if the state is not known
        """
        return self._light_tsr_enabled

    def get_light_tsr(self):
        """
        Get light TSR value.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            Integer lux
        """
        return self._light_tsr

    def _request_light_tsr_event(self, timeout=DEFAULT_TIMEOUT):
        """
        Send a REST GET request to the server to get the last light TSR
        value as reported by the sensor.

        It does not contact the node directly, reducing network traffic and
        increasing battery life. The data is updated as defined by the node's
        sample period.

        Args:
            timeout: Time to wait for the node response

        Returns:
            A dom object with the light TSR as lux
        """
        param = ("method=events.readLast&name=LightTSRReadEvent&addr="
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def _request_light_tsr_event_expensive(self, timeout=10):
        """
        Send a REST GET request to the node to get the current light TSR value.

        It contacts the node directly, polluting the network and reducing
        battery life. On the other hand it gets as up-to-date data as possible
        without waiting for the update to be triggered by the sample period.

        Args:
            timeout: Time to wait for the node response

        Returns:
            A dom object with the light TSR as lux
        """
        param = ("method=attributes.get&name=LightTSRReadEvent&addr="
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def _parse_light_tsr_event(self, dom):
        """
        Parse a dom object contatining the light TSR recorded by the sensor.

        Args:
            dom: The object containing the light TSR as lux

        Returns:
            Light TSR as a integer lux
        """
        value_tag = dom.getElementsByTagName("Value")
        if len(value_tag) == 0:
            return None

        return int(value_tag[0].firstChild.toxml())

    def update_light_tsr(self, timeout=DEFAULT_TIMEOUT):
        """
        Get the last light TSR vlaue from the server as reported by the sensor
        and store the value.

        It does not contact the node directly, reducing network traffic and
        increasing battery life. The data at the server is updated as defined
        by the node's sample rate.

        Args:
            timeout: Time to wait for the node response.
        """
        dom_tsr = self._request_light_tsr_event(timeout)
        self._light_tsr = self._parse_light_tsr_event(dom_tsr)

    def update_light_tsr_expensive(self, timeout=DEFAULT_TIMEOUT):
        """
        Get current light TSR from the sensor and store the value.

        It contacts the node directly, polluting the netowork and reducing
        battery life. On the other hand it gets as up-to-date data as possible
        without waiting for the update to be triggered by the sample period.

        Args:
            timeout: Time to wait for the node response.
        """
        dom_tsr = self._request_light_tsr_event_expensive(timeout)
        self._light_tsr = self._parse_light_tsr_event(dom_tsr)

    def is_light_par_enabled(self):
        """
        Check whether the light PAR sensor is enabled.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            True when the sensor is enabled,
            False if not,
            None if the state is not known
        """
        return self._light_par_enabled

    def get_light_par(self):
        """
        Get light PAR value.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            Integer lux
        """
        return self._light_par

    def _request_light_par_event(self, timeout=DEFAULT_TIMEOUT):
        """
        Send a REST GET request to the server to get the last light PAR value
        reported by the sensor.

        It does not contact the node directly, reducing network traffic and
        increasing battery life. The data at the server is updated as defined
        by the node's sample period.

        Args:
            timeout: Time to wait for the node response

        Returns:
            A dom object with the light PAR as lux
        """
        param = ("method=events.readLast&name=LightPARReadEvent&addr="
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def _request_light_par_event_expensive(self, timeout=DEFAULT_TIMEOUT):
        """
        Send a REST GET request to the node to get the current light PAR
        from the sensor.

        It contacts the node directly, polluting the network and reducing
        battery life. On the other hand it gets as up-to-date data as possible.

        Args:
            timeout: Time to wait for the node response

        Returns:
            A dom object with the light PAR as lux
        """
        param = ("method=attributes.get&name=LightPARReadEvent&addr="
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def _parse_light_par_event(self, dom):
        """
        Parse a dom object contatining the light PAR recorded by the sensor.

        Args:
            dom: The object containing the light PAR as lux

        Returns:
            Light PAR as a integer lux
        """
        value_tag = dom.getElementsByTagName("Value")
        if len(value_tag) == 0:
            return None

        return int(value_tag[0].firstChild.toxml())

    def update_light_par(self, timeout=DEFAULT_TIMEOUT):
        """
        Get the last light PAR value from the server reported by the sensor,
        and store that value.

        It does not contact the node directly, reducing the network traffic and
        increasing battery life. The data at the server is updated as defined
        by the node's sample period.

        Args:
            timeout: Time to wait for the node response.
        """
        dom_par = self._request_light_par_event(timeout)
        self._light_par = self._parse_light_par_event(dom_par)

    def update_light_par_expensive(self, timeout=DEFAULT_TIMEOUT):
        """
        Get current light PAR from the sensor and store the value.

        It contacts the node directly, polluting the network and reducing
        battery life. On the other hand it gets as up-to-date data as possible
        without waiting for the update triggered by the node's sample period.

        Args:
            timeout: Time to wait for the node response.
        """
        dom_par = self._request_light_par_event_expensive(timeout)
        self._light_par = self._parse_light_par_event(dom_par)

    def get_port_state(self, port):
        """
        Get the state of a given port.

        The state can be either 0 (low voltage) or 1 (high voltage).

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            port: The name of the port (string)

        Returns:
            0 or 1
        """
        if port == "Switch":
            return self._switch
        elif port == "IO0":
            return self._IO0
        elif port == "IO1":
            return self._IO1
        elif port == "IO2":
            return self._IO2
        else:
            logging.warning("Unknown port")

    def _set_port_state(self, port, state):
        """
        Set the state of a give port.

        The state can be either 0 (low voltage) or 1 (high voltage).

        This method does not communicate with the node directly.
        Run separate methods to send this value to the node.

        Args:
            port: The name of the port (string)
            state: 0 or 1
        """
        if state != 0 and state != 1:
            logging.warning("State must be 0 or 1")
            return
        if port == "Switch":
            self._switch = state
        elif port == "IO0":
            self._IO0 = state
        elif port == "IO1":
            self._IO1 = state
        elif port == "IO2":
            self._IO2 = state
        else:
            logging.warning("Unknown port")

    def is_port_enabled(self, port):
        """
        Check whether a given port is enabled.

        The state can be either 0 (low voltage) or 1 (high voltage).

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            port: The name of a port as string.

        Returns:
            0 or 1

        """
        if port == "Switch":
            return self._switch_enabled
        elif port == "IO0":
            return self._IO0_enabled
        elif port == "IO1":
            return self._IO1_enabled
        elif port == "IO2":
            return self._IO2_enabled
        else:
            logging.warning("Unknown port")

    def enable_port(self, port, boolean):
        """
        Enable / disable a given port.

        This method does not communicate with the node directly.
        Run separate methods to send this value to the node.

        Args:
            port: The name of a port as string.
            boolean: True to enable, False to disable.
        """
        if type(boolean) != bool:
            logging.warning("Not a boolean value")
            return
        if port == "Switch":
            self._switch_enabled = boolean
        elif port == "IO0":
            self._IO0_enabled = boolean
        elif port == "IO1":
            self._IO1_enabled = boolean
        elif port == "IO2":
            self._IO2_enabled = boolean
        else:
            logging.warning("Unknown port")

    def is_port_sampling(self, port):
        """
        Check whether a given port is sampling the data.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            port: The name of a port as string.

        Returns:
            True if the port is sampling, False if it is not.
        """
        if port == "Switch":
            return self._switch_sampling
        elif port == "IO0":
            return self._IO0_sampling
        elif port == "IO1":
            return self._IO1_sampling
        elif port == "IO2":
            return self._IO2_sampling
        else:
            logging.warning("Unknown port")

    def enable_port_sampling(self, port, boolean):
        """
        Enable / disable port sampling.

        This method does not communicate with the node directly.
        Run separate methods to send this value to the node.

        Args:
            port: The name of a port as string
            boolean: True to enable, False to disable
        """
        if type(boolean) != bool:
            logging.warning("True and False accepted")
            return
        if port == "Switch":
            self._switch_sampling = boolean
        elif port == "IO0":
            self._IO0_sampling = boolean
        elif port == "IO1":
            self._IO1_sampling = boolean
        elif port == "IO2":
            self._IO2_sampling = boolean
        else:
            logging.warning("Unknown port")

    def is_port_interrupting(self, port):
        """
        Check whether the port is sending interrupts on state change.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            port: The name of a port as string

        Returns:
            True if it is interrupting, False if it is not.
        """
        if port == "Switch":
            return self._switch_interrupting
        elif port == "IO0":
            return self._IO0_interrupting
        else:
            return False

    def enable_port_interrupting(self, port, boolean):
        """
        Enable / disable sending interrupts on state change.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            port: The name of the port as string.
            boolean: True to enable, False to disable.
        """
        if type(boolean) != bool:
            logging.warning("Only boolean values accepted")
            return
        if port == "Switch":
            self._switch_interrupting = boolean
        elif port == "IO0":
            self._IO0_interrupting = boolean
        #else:
        #    logging.warning("Port " +str(port)+ " does not support interrupts")

    def is_port_used_as_output(self, port):
        """
        Check if the port is used as output or input.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            port: The name of the port as string

        Returns:
            True if the port is used as output, False if as input
        """
        if port == "IO0":
            return self._IO0_as_output
        elif port == "IO1":
            return self._IO1_as_output
        elif port == "IO2":
            return self._IO2_as_output
        else:
            return False

    def enable_port_as_output(self, port, boolean):
        """
        Set the port to output or input mode.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            port: The name of the port as string
            boolean: True to enable output, False to enable input.
        """
        if type(boolean) != bool:
            logging.warning("Only boolean values accepted")
            return
        if port == "IO0":
            self._IO0_as_output = boolean
        elif port == "IO1":
            self._IO1_as_output = boolean
        elif port == "IO2":
            self._IO2_as_output = boolean
        #else:
        #    logging.warning("Port " + str(port) + " does not support output")

    def _request_port_config(self, port, timeout):
        """
        Request the configuration of a port from the node.

        Args:
            port: The name of the port as string
            timeout: Time to wait for the node's response.

        Returns:
            The dom object containing the config
        """
        param = ("method=attributes.get&name=" + port + "Config"
                 + "&addr=" + self.get_address()
                 + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def _parse_port_config(self, port, dom):
        """
        Parse the config response from the node and update object's data.

        Args:
            port: The name of the port as string
            dom: The dom object containing the Config response
        """
        value_tag = dom.getElementsByTagName("Value")
        if len(value_tag) == 0:
            return

        enabled_tag = value_tag[0].getElementsByTagName("enabled")
        enabled = int(enabled_tag[0].firstChild.toxml())

        sample_tag = value_tag[0].getElementsByTagName("sample")
        sample = int(sample_tag[0].firstChild.toxml())

        interrupt_tag = value_tag[0].getElementsByTagName("interrupt")
        interrupt = int(interrupt_tag[0].firstChild.toxml())

        output_tag = value_tag[0].getElementsByTagName("output")
        output = int(output_tag[0].firstChild.toxml())

        self.enable_port(port, False)
        self.enable_port_sampling(port, False)
        self.enable_port_interrupting(port, False)
        self.enable_port_as_output(port, False)

        if enabled == 1:
            self.enable_port(port, True)

        if sample == 1:
            self.enable_port_sampling(port, True)

        if interrupt == 1:
            self.enable_port_interrupting(port, True)

        if output == 1:
            self.enable_port_as_output(port, True)

    def update_port_config(self, port, timeout=DEFAULT_TIMEOUT):
        """
        Send a config request to the node, and update the state of the object.

        Args:
            port: The name of the port as string
            timeout: Time to wait for the node's reponse.
        """
        dom = self._request_port_config(port, timeout)
        self._parse_port_config(port, dom)

    def send_port_config(self, port):
        """
        Send the current port configuration to the node.

        Args:
            port: The name of the port as string.

        Returns:
            The dom object containing the reponse from the node (Config)
        """
        enabled = 0
        if self.is_port_enabled(port):
            enabled = 1

        sampling = 0
        if self.is_port_sampling(port):
            sampling = 1

        interrupting = 0
        if self.is_port_interrupting(port):
            interrupting = 1

        output = 0
        if self.is_port_used_as_output(port):
            output = 1

        param = ("method=attributes.set&name=" + port + "Config"
                 + "&value.pulseWidth=0"
                 + "&value.pulseFalling=0"
                 + "&value.output=" + str(output)
                 + "&addr=" + self.get_address()
                 + "&value.enabled=" + str(enabled)
                 + "&value.sample=" + str(sampling)
                 + "&value.interrupt=" + str(interrupting))

        dom = self._conn.REST_GET(param)
        self._parse_port_config(port, dom)
        return dom

    def update_all_ports_config(self, timeout=DEFAULT_TIMEOUT):
        """
        Update the configuration of all the ports.

        Args:
            timeout: time to wait for the node's response.
        """
        for port in self._ports:
            self.update_port_config(port, timeout)

    def send_all_ports_config(self):
        """
        Send the configuration of all ports to the node.
        """
        for port in self._ports:
            self.send_port_config(port)

    def _request_port_event(self, port, timeout=DEFAULT_TIMEOUT):
        """
        Send a REST GET request to the server to get the last state
        of the port.

        It does not contact the node directly, reducing network traffic and
        increasing battery life. The data as the server is updated as defined
        by the node's sample period, and also whenever the state of the port
        changes (if interrupts have not been disabled, and are possible)

        Args:
            timeout: Time to wait for the node response

        Returns:
            A dom object with the switch state (1/0)
        """
        param = ("method=events.readLast&name=" + port + "ReadEvent&addr="
                 + self.get_address() + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

#    def _request_port_event_expensive(self, port, timeout=DEFAULT_TIMEOUT):
#        """
#        Send a REST GET request to the node to get the port state.
#        
#        Use of the method is discouraged. It contacts the node directly, 
#        polluting the network and reducing the battery life.
#        This method is useful if a port does not support interupts (IO1, IO2),
#        and you need as recent reading as possible. 
#        If the port supports interrupts, then it is useful only in case
#        a message from the node is lost on its way to the server, and the 
#        reading is not available.
#        
#        Args:
#            timeout: Time to wait for the node response
#            
#        Returns:
#            A dom object with the state of the port
#        """
#        param = ("method=attributes.get&name=" + port + "ReadEvent&addr="
#                 + self.get_address() + "&timeout=" + str(timeout))
#        return self._conn.REST_GET(param)

    def _parse_port_event(self, dom):
        """
        Parse a dom object contatining the port state.

        Args:
            dom: The object containing the port state.

        Returns:
            1/0 port reading
        """
        value_tag = dom.getElementsByTagName("Value")
        if len(value_tag) == 0:
            return None

        return int(value_tag[0].firstChild.toxml())

    def update_port(self, port, timeout=DEFAULT_TIMEOUT):
        """
        Get current state of the port from the server and store the value.

        Does not contact the node directly, reducing network traffic and
        increasing battery life.

        Args:
            timeout: Time to wait for the node response.
        """
        dom = self._request_port_event(port, timeout)
        self._set_port_state(port, self._parse_port_event(dom))

#    def update_port_expensive(self, port, timeout=DEFAULT_TIMEOUT):
#        """
#        Get the current port state from the sensor and store the value.
#        
#        Use of this method is discouraged, since it pollutes the network
#        and reduces the battery life. Use only if you need the most recent
#        reading from a port that does not support interrupts, or if you think
#        that a reading was lost on its way from the node to the server.
#        
#        Args:
#            timeout: Time to wait for the node response.
#        """
#        dom = self._request_port_event_expensive(port, timeout)
#        self._set_port_state(port, self._parse_port_event(dom))


class IPPower(Node):
    """
    Manages the 3 circuit power meter.
    """

    _phases_enabled = [True, True, True] # and do not change it
    _CT_amps = []
    _nominal_volts = []

    _dVolts = [] # voltage in deci volt
    _dAmps = [] # current in deci amp
    _dWatts = [] # power in deci watt
    _dVAR = [] # reactive power in deci watt
    _dVA = [] # apparent power in deci watt

    _dHz = None # line frequency in deci hertz

    _max_freq = None
    _min_freq = None
    _max_freq_time = None
    _min_freq_time = None

    _max_volts = []
    _min_volts = []
    _max_volts_times = []
    _min_volts_times = []

    _max_delivered_real_power = []
    _max_received_real_power = []
    _max_delivered_real_power_times = []
    _max_received_real_power_times = []

    _max_lead_power = []
    _max_lag_power = []
    _max_lead_power_times = []
    _max_lag_power_times = []

    _max_apparent_powers = []
    _max_currents = []
    _max_apparent_powers_times = []
    _max_currents_times = []


    def __init__(self, addr, conn):
        super(IPPower, self).__init__(addr, conn)

    def are_phases_enabled(self):
        """
        Check whether the meters are enabled.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of 3 booleans showing whether the meters are enabled or not.
        """
        return self._phases_enabled

    def enable_phases(self, bool_arr):
        """
        Enable / disable the power meters.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            bool_arr: A list of 3 booleans specifying which phases should be on.
        """
        if len(bool_arr) != 3:
            logging.warning("Wrong number of phases")
            return
        self._phases_enabled = bool_arr

    def get_current_transformer_amps(self):
        """
        Get the maximum safe current of the transformers.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of 3 max currents in amps
        """
        return self._CT_amps

    def set_current_transformer_amps(self, int_arr):
        """
        Set the maximum safe current of the transformers.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            int_arr: A list of 3 max currents in amps
        """
        if len(int_arr) != 3:
            logging.warning("Wrong number of values")
            return
        self._CT_amps = int_arr

    def get_nominal_volts(self):
        """
        Get the nominal voltages of each circuit.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of 3 voltages in volts
        """
        return self._nominal_volts

    def set_nominal_volts(self, int_arr):
        """
        Set the nominal voltages of each circuit.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            int_arr: A list of 3 voltages in volts.
        """
        if len(int_arr) != 3:
            logging.warning("Wrong number of values")
            return
        self._nominal_volts = int_arr

    def get_voltages(self):
        """
        Get the voltage readings.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of three voltages in deciVolts as ints.
        """
        return self._dVolts

    def _set_voltages(self, int_arr):
        """
        Set the voltages.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            int_arr: A list of three integers with deciVolts as ints
        """
        if len(int_arr) != 3:
            logging.warning("Wrong number of voltages")
            return
        self._dVolts = int_arr

    def get_currents(self):
        """
        Get the electric current readings.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of three current readings as ints in deciAmps.
        """
        return self._dAmps

    def _set_currents(self, int_arr):
        """
        Set the electric current readings.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            int_arr: A list of three ints containing the current
            readings in deciAmps.
        """
        if len(int_arr) != 3:
            logging.warning("Wrong number of currents")
            return
        self._dAmps = int_arr

    def get_real_powers(self):
        """
        Get the real power readings from each circuit.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of three integers containing
            the real power readings in deciWatts.
        """
        return self._dWatts

    def _set_real_powers(self, int_arr):
        """
        Set the real powers.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            int_arr: A list of three integers contatining
            the real power readings in deciWatts.
        """
        if len(int_arr) != 3:
            logging.warning("Wrong number of real powers")
            return
        self._dWatts = int_arr

    def get_reactive_powers(self):
        """
        Get the readings of the reactive power from each circuit.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of three integers containing the reactive power
            readings in deciWatts.
        """
        return self._dVAR

    def _set_reactive_powers(self, int_arr):
        """
        Set the reactive powers.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            int_arr: A list of three integers contatning the
            reactive power readings in deciWatts.
        """
        if len(int_arr) != 3:
            logging.warning("Wing number of reactive powers")
            return
        self._dVAR = int_arr

    def get_apparent_powers(self):
        """
        Get the reading of apparent power from each circuit.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of three integers containing the apparent power
            readings in deciWatts.
        """
        return self._dVA

    def _set_apparent_powers(self, int_arr):
        """
        Set the apparent powers.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            int_arr: A list of three integers containing the
            aparent power readings in deciWatts.
        """
        if len(int_arr) != 3:
            logging.warning("Wrong number of apparent powers")
            return
        self._dVA = int_arr

    def get_line_frequency(self):
        """
        Get the electric frequency of the line.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            An integer containing the line frequency in deciHertz.
        """
        return self._dHz

    def _set_line_frequency(self, freq):
        """
        Set the frequency of the line.

        This method does not communicate with the node directly.
        Run separate methods to send this value to the node.

        Args:
            freq: An integer with line frequency as deciHertz
        """
        if type(freq) != int:
            logging.warning("Wrong frequency type")
            return
        self._dHz = freq

    def get_max_line_frequency(self):
        """
        Get the peak frequency observed by the node.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            An integer containing the maximum detected frequency in deciHz
        """
        return self._max_freq

    def _set_max_line_frequency(self, freq):
        """
        Set the peak frequency observed by the node,

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            freq: Maximum frequency reported by the node in deciHz
        """
        if type(freq) != int:
            logging.warning("Not an integer")
            return
        self._max_freq = freq

    def get_max_line_frequency_time(self):
        """
        Get the peak frequency observed by the node.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            An integer containing the time then the maximum frequency was
            detected. UNIX time
        """
        return self._max_freq_time

    def _set_max_line_frequency_time(self, t):
        """
        Set the peak frequency observed by the node.

        This method does not communicate with the node directly.
        Run separate methods to send this value to the node.

        Args:
            t: Timestamp when the max frequency was registered. UNIX time
        """
        if type(t) != int:
            logging.warning("Not an integer")
            return
        self._max_freq_time = t

    def get_min_line_frequency(self):
        """
        Get the peak frequency observed by the node.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            An integer containing the minumum frequency reported by the node.
            DeciHz
        """
        return self._min_freq

    def _set_min_line_frequency(self, freq):
        """
        Set the peak frequency observed by the node.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            freq: An int containing the minimum frequency reported by the node.
            DeciHz
        """
        if type(freq) != int:
            logging.warning("Not an integer")
            return
        self._min_freq = freq

    def get_min_line_frequency_time(self):
        """
        Get the time when the peak frequency was observed.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            An int containing the time when the lowest frequency was detected.
            UNIX timestamp
        """
        return self._min_freq_time

    def _set_min_line_frequency_time(self, t):
        """
        Set the time when the peak frequency was observed.

        This method does not communicate with the node directly.
        Run separate methods to send this value to the node.

        Args:
            t: UNIX timestamp when the lowest frequency was detected
        """
        if type(t) != int:
            logging.warning("Not an integer")
            return
        self._min_freq_time = t

    def get_max_voltages(self):
        """
        Get the time when the peak frequency was observed.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of three integers containing max voltages
            registered by the node in deciVolts.
        """
        return self._max_volts

    def _set_max_voltages(self, int_arr):
        """
        Set the time when the peak frequency was observed.

        This method does not communicate with the node directly.
        Run separate methods to send this value to the node.

        Args:
            int_arr: A list of three integers containing max voltages
            registered by the node in deciVolts.
        """
        if len(int_arr) != 3:
            logging.warning("Wrong number of voltages")
            return
        self._max_volts = int_arr

    def get_min_voltages(self):
        """
        Get the peak voltage observed at the node

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of three integers containing min voltages
            registered by the node in deciVolts.
        """
        return self._min_volts

    def _set_min_voltages(self, int_arr):
        """
        Set the peak voltage observed at the node

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            int_arr: A list of three integers containing min voltages
            registered by the node in deciVolts.
        """
        if len(int_arr) != 3:
            logging.warning("Wrong number of voltages")
            return
        self._min_volts = int_arr

    def get_max_voltages_times(self):
        """
        Get the peak voltage observed at the node

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of three UNIX timestamps containing times when max
            voltages were registered by the node.
        """
        return self._max_volts_times

    def _set_max_voltages_times(self, int_arr):
        """
        Set the peak voltage observed at the node

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            int_arr: A list of three UNIX timestamps containing times when max
            voltages were registered by the node.
        """
        if len(int_arr) != 3:
            logging.warning("Wrong number of timestamps")
            return
        self._max_volts_times = int_arr

    def get_min_voltages_times(self):
        """
        Get the peak voltage observed at the node

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of three UNIX timestamps containing times when min
            voltages were registered by the node.
        """
        return self._min_volts_times

    def _set_min_voltages_times(self, int_arr):
        """
        Set the peak voltage observed at the node

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            int_arr: A list of three UNIX timestamps containing times when min
            voltages were registered by the node.
        """
        if len(int_arr) != 3:
            logging.warning("Wrong number of timestamps")
            return
        self._min_volts_times = int_arr

    def get_max_delivered_real_power(self):
        """
        Get the peak power delivered to the load.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of 3 integers decsribing the max observed delivered power
            (from the source to the load) in deciWatts.
            Returns None if the readings are not known.
        """
        return self._max_delivered_real_power

    def _set_max_delivered_real_power(self, int_arr):
        """
        Set the peak power delivered to the load.

        This method does not communicate with the node directly.
        Run separate methods to send this value to the node.

        Args:
            int_arr: A list of 3 integers containing the readings of max
            delivered power from the source to the load. DeciWatts.
        """
        if len(int_arr) != 3:
            logging.warning("Wrong number of powers")
            return
        self._max_delivered_real_power = int_arr

    def get_max_received_real_power(self):
        """
        Get the peak power received from the load.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of 3 integers describing the max observed delivered power
            (from the source to the load) in deciWatts.
            Returns None if the readings are not known.
        """
        return self._max_received_real_power

    def _set_max_received_real_power(self, int_arr):
        """
        Set the peak power delivered to the load.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            int_arr: A list of 3 integers containing the readings of max
            received power from the load to the source. DeciWatts.
        """
        if len(int_arr) != 3:
            logging.warning("Wrong number of powers")
            return
        self._max_received_real_power = int_arr

    def get_max_delivered_real_power_times(self):
        """
        Get the the time when the peak power was delivered to the load.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of 3 integers containing the UNIX timestamps when the
            max delivered real power was observed.
            Returns None is the times are not known.
        """
        return self._max_delivered_real_power_times

    def _set_max_delivered_real_power_times(self, int_arr):
        """
        Set the the time when the peak power was delivered to the load.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            int_arr: A list of 3 integers contatining the UNIX timestamps
            when the max relivered real power wass observed.
        """
        if len(int_arr) != 3:
            logging.warning("Wrong number of timestamps")
            return
        self._max_delivered_real_power_times = int_arr

    def get_max_received_real_power_times(self):
        """
        Get the the time when the peak power was received from the load.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of 3 integers describing then the max received real power
            was observed.
            Returns None if the readings are not known.
        """
        return self._max_received_real_power_times

    def _set_max_received_real_power_times(self, int_arr):
        """
        Set the the time when the peak power was received from the load.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            int_arr: A list of 3 integers describing when the max received real
            power was observed.
        """
        if len(int_arr) != 3:
            logging.warning("Wrong number of timestamps")
            return
        self._max_received_real_power_times = int_arr

    def get_max_lead_power(self):
        """
        Get the peak value of the leading power observed by the node

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of 3 integers decribing the max leading power observed
            by the node. DeciWatts.
            Returns None if the readings are not known.
        """
        return self._max_lead_power

    def _set_max_lead_power(self, int_arr):
        """
        Set the peak value of the leading power observed by the node.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            int_arr: A list of 3 integers describing the max leading power
            observed by the node. DeciWatts.
        """
        if len(int_arr) != 3:
            logging.warning("Wrong number of powers")
            return
        self._max_lead_power = int_arr

    def get_max_lag_power(self):
        """
        Get the peak value of the lagging power observed by the node,

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of 3 integers describing max lagging power observed by
            the node. DeciWatts.
            Returns None if the readings are not known.
        """
        return self._max_lag_power

    def _set_max_lag_power(self, int_arr):
        """
        Set the peak value of lagging power obesrved by the node,

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            int_arr: A list of 3 integers describing max lagging power observed
            by the node. DeciWatts.
        """
        if len(int_arr) != 3:
            logging.warning("Wrong number of powers")
            return
        self._max_lag_power = int_arr

    def get_max_lead_power_times(self):
        """
        Get the time when the peak leading power was observed,

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of 3 integers containing the UNIX timestamps when
            then max leading power was observed.
            Returns None if the times are not known.
        """
        return self._max_lead_power_times

    def _set_max_lead_power_times(self, int_arr):
        """
        Set the time when the peak leading power was observed.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            int_arr: A list of 3 integers containing the timestamps when
            max leading power was observed.
        """
        if len(int_arr) != 3:
            logging.warning("Wrong number of timestamps")
            return
        self._max_lead_power_times = int_arr

    def get_max_lag_power_times(self):
        """
        Get the time when the peak lagging power was observed.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of 3 integers containing the UNIX timestamps
            when the max lagging power was observed. DeciWatts.
            Returns None if the readings are not known.
        """
        return self._max_lag_power_times

    def _set_max_lag_power_times(self, int_arr):
        """
        Set the time when the peak lagging power was observed.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            int_arr: A list of 3 integers containing the UNIX timestamps
            when the max lagging power was observed.
        """
        if len(int_arr) != 3:
            logging.warning("Wrong number of timestamps")
            return
        self._max_lag_power_times = int_arr

    def get_max_apparent_powers(self):
        """
        Get the maximum apparent power observed at each circuit.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of 3 integers containing the max apparent power
            observed by each element of the sensor node. DeciWatts.
            Returns None if the readings are not known.
        """
        return self._max_apparent_powers

    def _set_max_apparent_powers(self, int_arr):
        """
        Set the maximum apparent power observed at each circuit.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            int_arr: A list of 3 integers containing the max apparent power
            readings. DeciWatts.
        """
        if len(int_arr) != 3:
            logging.warning("Wrong number of powers")
            return
        self._max_apparent_powers = int_arr

    def get_max_currents(self):
        """
        Get the maximum electric current observed at each circuit.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of 3 integers containing the max current observed by the
            node. DeciAmps.
            Returns None if the readings are not known.
        """
        return self._max_currents

    def _set_max_currents(self, int_arr):
        """
        Set the maximum electric current observed at each circuit.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            int_arr: A list of 3 integers containing the max current observed
            by the node. DeciAmps.
        """
        if len(int_arr) != 3:
            logging.warning("Wrong number of currents")
            return
        self._max_currents = int_arr

    def get_max_apparent_powers_times(self):
        """
        Get the time when the max apparent power was observed.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of 3 integers containing UNIX timestamps when the
            max apparent powers were observed.
            Returns None if the times are not known.
        """
        return self._max_apparent_powers_times

    def _set_max_apparent_powers_times(self, int_arr):
        """
        Set the time when the max apparent power was observed.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            int_arr: A list of 3 integers containing the UNIX timestamps
            when the max apparent power were observed.
        """
        if len(int_arr) != 3:
            logging.warning("Wrong number of timestamps")
            return
        self._max_apparent_powers_times = int_arr

    def get_max_currents_times(self):
        """
        Get the times when the max currents were observed.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Returns:
            A list of 3 integers containing the timestamps when the max
            currents wetre observed.
            Returns None if the times are not known.
        """
        return self._max_currents_times

    def _set_max_currents_times(self, int_arr):
        """
        Set the times when the max currents were observed.

        This method does not communicate with the node directly.
        Run separate methods to update this value.

        Args:
            int_arr: A list of 3 integers containing the UNIX timestamps when
            the max currents wetre observed at the node.
        """
        if len(int_arr) != 3:
            logging.warning("Wrong number of timestamps")
            return
        self._max_currents_times = int_arr

    def read_config(self, timeout):
        """
        Send a REST GET to the node to obtain the Config response.

        Args:
             timeout: Time to wait for the node response (sec)

        Returns:
            A dom object containg the Config response.
        """
        param = ("method=attributes.get&name=ElectricalConfig&addr="
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def _parse_config(self, dom):
        """
        Parse a Config response from the node and update the object.

        Args:
            dom: The Config response from the node.
        """
        value_tag = dom.getElementsByTagName("Value")
        if len(value_tag) == 0:
            logging.warning("No value")
            return

        CTs = []
        volts = []

        elements = value_tag[0].getElementsByTagName("Element")
        for element in elements:
            # do not parse the 'flags' field. it does not change anything

            CT = int(element.getElementsByTagName("CTAmps")[0]
                     .firstChild.toxml())
            CTs.append(CT)

            volt = int(element.getElementsByTagName("nominalVolts")[0]
                       .firstChild.toxml())
            volts.append(volt)

        self.set_current_transformer_amps(CTs)
        self.set_nominal_volts(volts)

    def update_config(self, timeout=DEFAULT_TIMEOUT):
        """
        Ask the node for the current configuration
        and update the internal state.

        Args:
             timeout: Time to wait for the node response (sec)
        """
        dom = self.read_config(timeout)
        self._parse_config(dom)

    def send_config(self, timeout=DEFAULT_TIMEOUT):
        """
        Send a Config message to the node to reflect the object's state.

        Args:
            timeout: Time to wait for the node's response

        Returns:
            The Config dom object returned by the node.
        """

        CTs = self.get_current_transformer_amps()
        volts = self.get_nominal_volts()

        param = ("method=attributes.set"
                 + "&addr=" + self.get_address()
                 + "&timeout=" + str(timeout)
                 + "&name=ElectricalConfig"
                 + "&value.phases.element0.flags=1"
                 + "&value.phases.element0.CTAmps=" + str(CTs[0])
                 + "&value.phases.element0.nominalVolts=" + str(volts[0])
                 + "&value.phases.element1.flags=1"
                 + "&value.phases.element1.CTAmps=" + str(CTs[1])
                 + "&value.phases.element1.nominalVolts=" + str(volts[1])
                 + "&value.phases.element2.flags=1"
                 + "&value.phases.element2.CTAmps=" + str(CTs[2])
                 + "&value.phases.element2.nominalVolts=" + str(volts[2]))

        dom = self._conn.REST_GET(param)
        return dom

    def read_sensors(self, timeout=DEFAULT_TIMEOUT):
        """
        Send a REST GET to the server to get the last reading reported by
        the node.

        The node is not contacted. It has a benefit over the
        'read_sensors_expensive' as it does not pollute network, and also
        it helps to reduce battery drain. Nodes report the readings as defined
        by the sample period value.

        Args:
            timeout: Time to wait for the node response (sec)

        Returns:
            A dom object containg the MultiElectricalDemand response.
        """
        param = ('method=events.readLast&name=MultiElectricalDemand&addr='
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def read_sensors_expensive(self, timeout=DEFAULT_TIMEOUT):
        """
        Send a REST GET to the node to obtain the current state.

        Contacts the node directly, polluting the network and reducing battery
        life. On the other hand gets as up-to-date data as possible.

        Args:
            timeout: Time to wait for the node response (sec)

        Returns:
            A dom object containg the MultiElectricalDemand response.
        """
        param = ('method=attributes.get&name=MultiElectricalDemand&addr='
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def _parse_MultiElectricalDemand(self, dom):
        """
        Parse the MultielectricalDemand response from the IPPower node
        and set appropriate internal values.

        Args:
            dom: A dom object contatining the MultiElectricalDemand event
        """
        value_tag = dom.getElementsByTagName("Value")
        if len(value_tag) == 0:
            logging.warning("No data")
            return

        volts = []
        currents = []
        real_powers = []
        react_powers = []
        apparent_powers = []

        freq_tag = value_tag[0].getElementsByTagName("dhertz")
        freq = int(freq_tag[0].firstChild.toxml())

        elements = value_tag[0].getElementsByTagName("Element")
        for element in elements:
            v = int(element.getElementsByTagName("dvolts")[0]
                    .firstChild.toxml())
            a = int(element.getElementsByTagName("damps")[0]
                    .firstChild.toxml())
            w = int(element.getElementsByTagName("dw")[0]
                    .firstChild.toxml())
            r = int(element.getElementsByTagName("dvar")[0]
                    .firstChild.toxml())
            app = int(element.getElementsByTagName("dva")[0]
                      .firstChild.toxml())

            volts.append(v)
            currents.append(a)
            real_powers.append(w)
            react_powers.append(r)
            apparent_powers.append(app)

        self._set_line_frequency(freq)
        self._set_voltages(volts)
        self._set_currents(currents)
        self._set_real_powers(real_powers)
        self._set_reactive_powers(react_powers)
        self._set_apparent_powers(apparent_powers)

    def update_data(self, timeout=DEFAULT_TIMEOUT):
        """
        Send a request for the last MultiElectricalDemand event reported to
        server and parse the response.

        It does not contact the node directly, therefore it reduces network
        traffic and increases battery life.

        Args:
            timeout: Time to wait for the servers response.
        """
        dom = self.read_sensors(timeout)
        self._parse_MultiElectricalDemand(dom)

    def update_data_expensive(self, timeout=DEFAULT_TIMEOUT):
        """
        Send a request for the MultiElectricalDemand event to the node
        and parse the response.

        It contacts the node directly, polluting the network and decreaseing
        battery life. Its use is discourages if you do not need the most
        recent data available to the sensor.

        Args:
            timeout: Time to wait fot the node's response.
        """
        dom = self.read_sensors_expensive(timeout)
        self._parse_MultiElectricalDemand(dom)

    def read_frequency_peaks(self, timeout=DEFAULT_TIMEOUT):
        """
        Send a REST GET to the server to get the last reading reported by
        the node.

        The node is not contacted. It has a benefit over the
        'read_frequency_peaks_expensive' as it does not pollute network,
        and it also helps to reduce battery drain. Nodes report the readings
        as often as defined by the sample period value.

        Args:
            timeout: Time to wait for the node response (sec)

        Returns:
            A dom object containg the ElectricalFreqPeak response.
        """
        param = ('method=events.readLast&name=ElectricalFreqPeak&addr='
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def read_frequency_peaks_expensive(self, timeout=DEFAULT_TIMEOUT):
        """
        Send a REST GET to the node to obtain the current state.

        Contacts the node directly, polluting the network and reducing battery
        life. On the other hand gets as up-to-date data as possible.

        Args:
            timeout: Time to wait for the node response (sec)

        Returns:
            A dom object containg the ElectricalFreqPeak response.
        """
        param = ('method=attributes.get&name=ElectricalFreqPeak&addr='
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def _parse_frequency_peaks(self, dom):
        """
        Parse a ElectricalFreqPeak response.

        Args:
            dom: A dom object containing the ElectricalFreqPeak response.
        """
        value_tag = dom.getElementsByTagName("Value")
        if len(value_tag) == 0:
            logging.warning("No data")
            return

        max_freq = int(value_tag[0].getElementsByTagName("max_dhertz")[0]
                       .firstChild.toxml())
        min_freq = int(value_tag[0].getElementsByTagName("min_dhertz")[0]
                       .firstChild.toxml())
        max_freq_time = int(value_tag[0].getElementsByTagName("max_tstamp")[0]
                            .firstChild.toxml())
        min_freq_time = int(value_tag[0].getElementsByTagName("min_tstamp")[0]
                            .firstChild.toxml())

        self._set_max_line_frequency(max_freq)
        self._set_min_line_frequency(min_freq)
        self._set_max_line_frequency_time(max_freq_time)
        self._set_min_line_frequency_time(min_freq_time)

    def update_frequency_peaks(self, timeout=DEFAULT_TIMEOUT):
        """
        Request an ElectricalFreqPeak event form the server
        and update internal state.

        Does not contact the node directly, does not pollute network
        and saves battery.

        Args:
            timeout: Time to wait for the event response
        """
        dom = self.read_frequency_peaks(timeout)
        self._parse_frequency_peaks(dom)

    def update_frequency_peaks_expensive(self, timeout=DEFAULT_TIMEOUT):
        """
        Request an ElectricalFreqPeak event from the node
        and update internal state.

        Contacts the node directly, pollutes the network
        and decreases battery life. Its use is discourages.
        Use only when you need the most up-to-data available to the node.

        Args:
            timeout: Time to wait for the node's response.
        """
        dom = self.read_frequency_peaks_expensive(timeout)
        self._parse_frequency_peaks(dom)

    def read_voltages_peaks(self, timeout=DEFAULT_TIMEOUT):
        """
        Send a REST GET to the server to get the last reading reported by
        the node.

        The node is not contacted. It has a benefit over the
        'read_frequency_peaks_expensive' as it does not pollute network,
        and it also helps to reduce battery drain. Nodes report the readings
        as often as defined by the sample period value.

        Args:
            timeout: Time to wait for the node response (sec)

        Returns:
            A dom object containg the ElectricalVoltPeak response.
        """
        param = ('method=events.readLast&name=ElectricalVoltPeak&addr='
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def read_voltages_peaks_expensive(self, timeout=DEFAULT_TIMEOUT):
        """
        Send a REST GET to the node to obtain the voltage peaks.

        Contacts the node directly, polluting the network and reducing battery
        life. On the other hand gets as up-to-date data as possible.

        Args:
            timeout: Time to wait for the node response (sec)

        Returns:
            A dom object containg the ElectricalVoltPeak response.
        """
        param = ('method=attributes.get&name=ElectricalVoltPeak&addr='
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def _parse_voltages_peaks(self, dom):
        """
        Parse a ElectricalVoltPeak response.

        Args:
            dom: A dom object containing the ElectricalVoltPeak response.
        """
        value_tag = dom.getElementsByTagName("Value")
        if len(value_tag) == 0:
            logging.warning("No data")
            return

        max_volts = []
        min_volts = []
        max_volts_times = []
        min_volts_times = []

        elements = value_tag[0].getElementsByTagName("Element")
        for element in elements:
            max_v = int(element.getElementsByTagName("max_dvolts")[0]
                        .firstChild.toxml())
            min_v = int(element.getElementsByTagName("min_dvolts")[0]
                        .firstChild.toxml())
            max_vt = int(element.getElementsByTagName("max_tstamp")[0]
                         .firstChild.toxml())
            min_vt = int(element.getElementsByTagName("min_tstamp")[0]
                         .firstChild.toxml())
            max_volts.append(max_v)
            min_volts.append(min_v)
            max_volts_times.append(max_vt)
            min_volts_times.append(min_vt)

        self._set_max_voltages(max_volts)
        self._set_min_voltages(min_volts)
        self._set_max_voltages_times(max_volts_times)
        self._set_min_voltages_times(min_volts_times)

    def update_voltages_peaks(self, timeout=DEFAULT_TIMEOUT):
        """
        Request an ElectricalVoltPeak event form the server
        and update internal state.

        Does not contact the node directly, does not pollute network
        and saves battery.

        Args:
            timeout: Time to wait for the event response
        """
        dom = self.read_voltages_peaks(timeout)
        self._parse_voltages_peaks(dom)

    def update_voltages_peaks_expensive(self, timeout=DEFAULT_TIMEOUT):
        """
        Request an ElectricalVoltPeak event from the node
        and update internal state.

        Contacts the node directly, pollutes the network
        and decreases battery life. Its use is discourages.
        Use only when you need the most up-to-data available to the node.

        Args:
            timeout: Time to wait for the node's response.
        """
        dom = self.read_voltages_peaks_expensive(timeout)
        self._parse_voltages_peaks(dom)

    def read_real_power_peaks(self, timeout=DEFAULT_TIMEOUT):
        """
        Send a REST GET to the server to get the last reading reported by
        the node.

        The node is not contacted. It has a benefit over the
        'read_real_power_peaks_expensive' as it does not pollute network,
        and it also helps to reduce battery drain. Nodes report the readings
        as often as defined by the sample period value.

        Args:
            timeout: Time to wait for the node response (sec)

        Returns:
            A dom object containg the ElectricalPwrPeak response.
        """
        param = ('method=events.readLast&name=ElectricalPwrPeak&addr='
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def read_real_power_peaks_expensive(self, timeout=DEFAULT_TIMEOUT):
        """
        Send a REST GET to the node to obtain the real power peaks.


        Contacts the node directly, polluting the network and reducing battery
        life. On the other hand gets as up-to-date data as possible.

        Args:
            timeout: Time to wait for the node response (sec)

        Returns:
            A dom object containg the ElectricalPwrPeak response.
        """
        param = ('method=attributes.get&name=ElectricalPwrPeak&addr='
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def _parse_real_power_peaks(self, dom):
        """
        Parse a ElectricalPwrPeak response.

        Args:
            dom: A dom object containing the ElectricalPwrPeak response.
        """
        value_tag = dom.getElementsByTagName("Value")
        if len(value_tag) == 0:
            logging.warning("No data")
            return

        max_dlvd = []
        max_rcvd = []
        max_dlvd_times = []
        max_rcvd_times = []

        elements = value_tag[0].getElementsByTagName("Element")
        for element in elements:
            max_d = int(element.getElementsByTagName("maxdlvd_dw")[0]
                        .firstChild.toxml())
            max_r = int(element.getElementsByTagName("maxrcvd_dw")[0]
                        .firstChild.toxml())
            max_d_t = int(element.getElementsByTagName("maxdlvd_tstamp")[0]
                         .firstChild.toxml())
            max_r_t = int(element.getElementsByTagName("maxrcvd_tstamp")[0]
                         .firstChild.toxml())
            max_dlvd.append(max_d)
            max_rcvd.append(max_r)
            max_dlvd_times.append(max_d_t)
            max_rcvd_times.append(max_r_t)

        self._set_max_delivered_real_power(max_dlvd)
        self._set_max_received_real_power(max_rcvd)
        self._set_max_delivered_real_power_times(max_dlvd_times)
        self._set_max_received_real_power_times(max_rcvd_times)

    def update_real_power_peaks(self, timeout=DEFAULT_TIMEOUT):
        """
        Request an ElectricalPwdPeak event form the server
        and update internal state.

        Does not contact the node directly, does not pollute network
        and saves battery.

        Args:
            timeout: Time to wait for the event response
        """
        dom = self.read_real_power_peaks(timeout)
        self._parse_real_power_peaks(dom)

    def update_real_power_peaks_expensive(self, timeout=DEFAULT_TIMEOUT):
        """
        Request an ElectricalPwrPeak event from the node
        and update internal state.

        Contacts the node directly, pollutes the network
        and decreases battery life. Its use is discourages.
        Use only when you need the most up-to-data available to the node.

        Args:
            timeout: Time to wait for the node's response.
        """
        dom = self.read_real_power_peaks_expensive(timeout)
        self._parse_real_power_peaks(dom)

    def read_reactive_power_peaks(self, timeout=DEFAULT_TIMEOUT):
        """
        Send a REST GET to the server to get the last reading reported by
        the node.

        The node is not contacted. It has a benefit over the
        'read_reactive_power_peaks_expensive' as it does not pollute network,
        and it also helps to reduce battery drain. Nodes report the readings
        as often as defined by the sample period value.

        Args:
            timeout: Time to wait for the node response (sec)

        Returns:
            A dom object containg the ElectricalReactPeak response.
        """
        param = ('method=events.readLast&name=ElectricalReactPeak&addr='
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def read_reactive_power_peaks_expensive(self, timeout=DEFAULT_TIMEOUT):
        """
        Send a REST GET to the node to obtain the reactive power peaks.

        Contacts the node directly, polluting the network and reducing battery
        life. On the other hand gets as up-to-date data as possible.

        Args:
            timeout: Time to wait for the node response (sec)

        Returns:
            A dom object containg the ElectricalReactPeak response.
        """
        param = ('method=attributes.get&name=ElectricalReactPeak&addr='
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def _parse_reactive_power_peaks(self, dom):
        """
        Parse a ElectricalReactPeak response.

        Args:
            dom: A dom object containing the ElectricalReactPeak response.
        """
        value_tag = dom.getElementsByTagName("Value")
        if len(value_tag) == 0:
            logging.warning("No data")
            return

        max_lead = []
        max_lag = []
        max_lead_times = []
        max_lag_times = []

        elements = value_tag[0].getElementsByTagName("Element")
        for element in elements:
            lead = int(element.getElementsByTagName("maxlead_dvar")[0]
                        .firstChild.toxml())
            lag = int(element.getElementsByTagName("maxlag_dvar")[0]
                        .firstChild.toxml())
            lead_t = int(element.getElementsByTagName("maxlead_tstamp")[0]
                         .firstChild.toxml())
            lag_t = int(element.getElementsByTagName("maxlag_tstamp")[0]
                         .firstChild.toxml())
            max_lead.append(lead)
            max_lag.append(lag)
            max_lead_times.append(lead_t)
            max_lag_times.append(lag_t)

        self._set_max_lead_power(max_lead)
        self._set_max_lag_power(max_lag)
        self._set_max_lead_power_times(max_lead_times)
        self._set_max_lag_power_times(max_lag_times)

    def update_reactive_power_peaks(self, timeout=DEFAULT_TIMEOUT):
        """
        Request an ElectricalReactPeak event form the server
        and update internal state.

        Does not contact the node directly, does not pollute network
        and saves battery.

        Args:
            timeout: Time to wait for the event response
        """
        dom = self.read_reactive_power_peaks(timeout)
        self._parse_reactive_power_peaks(dom)

    def update_reactive_power_peaks_expensive(self, timeout=DEFAULT_TIMEOUT):
        """
        Request an ElectricalReactPeak event from the node
        and update internal state.

        Contacts the node directly, pollutes the network
        and decreases battery life. Its use is discourages.
        Use only when you need the most up-to-data available to the node.

        Args:
            timeout: Time to wait for the node's response.
        """
        dom = self.read_reactive_power_peaks_expensive(timeout)
        self._parse_reactive_power_peaks(dom)

    def read_apparent_power_peaks(self, timeout=DEFAULT_TIMEOUT):
        """
        Send a REST GET to the server to get the last reading reported by
        the node.

        The node is not contacted. It has a benefit over the
        'read_apparent_power_peaks_expensive' as it does not pollute network,
        and it also helps to reduce battery drain. Nodes report the readings
        as often as defined by the sample period value.

        Args:
            timeout: Time to wait for the node response (sec)

        Returns:
            A dom object containg the ElectricalAppPeak response.
        """
        param = ('method=events.readLast&name=ElectricalAppPeak&addr='
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def read_apparent_power_peaks_expensive(self, timeout=DEFAULT_TIMEOUT):
        """
        Send a REST GET to the node to obtain the apparent power peaks.

        Contacts the node directly, polluting the network and reducing battery
        life. On the other hand gets as up-to-date data as possible.

        Args:
            timeout: Time to wait for the node response (sec)

        Returns:
            A dom object containg the ElectricalAppPeak response.
        """
        param = ('method=attributes.get&name=ElectricalAppPeak&addr='
                 + self._addr + "&timeout=" + str(timeout))
        return self._conn.REST_GET(param)

    def _parse_apparent_power_peaks(self, dom):
        """
        Parse a ElectricalAppPeak response.

        Args:
            dom: A dom object containing the ElectricalAppPeak response.
        """
        value_tag = dom.getElementsByTagName("Value")
        if len(value_tag) == 0:
            logging.warning("No data")
            return

        max_app = []
        max_cur = []
        max_app_times = []
        max_cur_times = []

        elements = value_tag[0].getElementsByTagName("Element")
        for element in elements:
            app = int(element.getElementsByTagName("maxs_dva")[0]
                        .firstChild.toxml())
            cur = int(element.getElementsByTagName("maxi_damps")[0]
                        .firstChild.toxml())
            app_t = int(element.getElementsByTagName("maxs_tstamp")[0]
                         .firstChild.toxml())
            cur_t = int(element.getElementsByTagName("maxi_tstamp")[0]
                         .firstChild.toxml())
            max_app.append(app)
            max_cur.append(cur)
            max_app_times.append(app_t)
            max_cur_times.append(cur_t)

        self._set_max_apparent_powers(max_app)
        self._set_max_currents(max_cur)
        self._set_max_apparent_powers_times(max_app_times)
        self._set_max_currents_times(max_cur_times)

    def update_apparent_power_peaks(self, timeout=DEFAULT_TIMEOUT):
        """
        Request an ElectricalAppPeak event form the server
        and update internal state.

        Does not contact the node directly, does not pollute network
        and saves battery.

        Args:
            timeout: Time to wait for the event response
        """
        dom = self.read_apparent_power_peaks(timeout)
        self._parse_apparent_power_peaks(dom)

    def update_apparent_power_peaks_expensive(self, timeout=DEFAULT_TIMEOUT):
        """
        Request an ElectricalAppPeak event from the node
        and update internal state.

        Contacts the node directly, pollutes the network
        and decreases battery life. Its use is discourages.
        Use only when you need the most up-to-data available to the node.

        Args:
            timeout: Time to wait for the node's response.
        """
        dom = self.read_apparent_power_peaks_expensive(timeout)
        self._parse_apparent_power_peaks(dom)


