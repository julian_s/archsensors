'''
Created on 2 Aug 2012

@author: julian
'''

import time


class Temperature_Detector(object):

    _sensors = []
    
    if __name__ == "__main__":
        print "This is not runnable"
    
    def add_sensor(self, sensor):
        self._sensors.append(sensor)
        
    def is_seat_occupied(self, sensor):
        temps = sensor.get_temperatures()
        print (temps)
        
        if (len(temps) != 7) or (temps[1] == None):
            return 'no data'
        
        if (temps[1] < 26) and (temps[2] > 28):
            return 'occupied'
        
        if (temps[1] < 26) and (temps[2] < 28):
            return 'free'
        
        return 'who knows'
    
    def detect(self, sleep_time):
        
        while True:
            
            print "Seats"
            
            for sensor in self._sensors:
                sensor.update_data()
                print (sensor.get_address() + ' ' 
                                  + str(self.is_seat_occupied(sensor)))

            time.sleep(sleep_time)
        
        
class Switch_Detector(object):
    
    _sensors = []
    
    def __init__(self):
        pass
    
    if __name__ == "__main__":
        print "this is not runnable"
        
    def add_sensor(self, sensor):
        self._sensors.append(sensor)
        
    def is_seat_occupied(self, sensor):
        if sensor.get_switch_state == 1:
            print sensor.get_address + " occupied"
            
    def detect(self, sleep_time):
        
        while True:
            
            print "Seats"
            
            for sensor in self._sensors:
                sensor.update_switch()
                print (sensor.get_address() + ' ' 
                                  + str(sensor.get_switch_state()))
            time.sleep(sleep_time)