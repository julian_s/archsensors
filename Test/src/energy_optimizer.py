'''
Created on 21 Aug 2012

@author: julian
'''

import ar_nodes
import connection
import logging

    
def disable_unused_IPThermal_6XT_ports(node):
    node.update_data_expensive(40)
    node.update_config(40)
    
    temps = node.get_temperatures()
    enabled = node.are_temperatures_enabled()
    not_redundant = [True] * 6
    for i in range(len(temps)):
        if temps[i] == None:
            not_redundant[i] = False
            
    if enabled == not_redundant:
        return
    
    node.enable_temperatures(not_redundant)
    node.send_config(40)
    
def disable_unused_IPThermal_3DP_ports(node):
    node.update_data_expensive(40)
    node.update_config(40)
    
    press = node.get_pressures()
    print "press " + str(press)
    enabled = node.are_pressures_enabled()
    print "enabled " + str(enabled)
    not_redundant = [True] * 3
    for i in range(len(press)):
        if press[i] == None:
            not_redundant[i] = False
            
    if enabled == not_redundant:
        return
    print "not red " + str(not_redundant)
    node.enable_pressures(not_redundant)
    node.send_config(40)
    
def disable_unused_ports(node):
    if type(node) == ar_nodes.IPThermal_RSN3040_6XT:
        disable_unused_IPThermal_6XT_ports(node)
    elif type(node) == ar_nodes.IPThermal_RSN3040_3DP:
        print "disable_unused_IPThermal_3DP_ports(node)"
        disable_unused_IPThermal_3DP_ports(node)
    else:
        logging.warning("unrecognized node")
        
def enable_all_IPThermal_6XT_ports(node):
    node.update_config(40)
    enabled = [True] * 6
    node.enable_temperatures(enabled)
    node.send_config(40)
    
def enable_all_IPThermal_3DP_ports(node):
    node.update_config(40)
    enabled = [True] * 3
    node.enable_pressures(enabled)
    node.send_config(40)
    
def enable_all_ports(node):
    if type(node) == ar_nodes.IPThermal_RSN3040_6XT:
        enable_all_IPThermal_6XT_ports(node)
    elif type(node) == ar_nodes.IPThermal_RSN3040_3DP:
        enable_all_IPThermal_3DP_ports(node)
    else:
        logging.warning("unrecognized node")
            
            
if __name__ == "__main__":
    conn = connection.Conn("guest", "veryTOPsecret", 
                       "http://areo.cs.ucl.ac.uk/gw/rest/V1?")
    xt = ar_nodes.IPThermal_RSN3040_6XT("00173b001380c58d", conn)
    dp = ar_nodes.IPThermal_RSN3040_3DP("00173b0013805b44", conn)
    
    nodes = [xt, dp]

    for node in nodes:
        enable_all_ports(node)
        disable_unused_ports(node)
                