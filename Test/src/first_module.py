'''
Created on 2 Aug 2012

@author: julian
'''



import connection
import ar_nodes
import butt_detector

conn = connection.Conn("guest", "veryTOPsecret", 
                       "http://areo.cs.ucl.ac.uk/gw/rest/V1?")


#node1 = ar_nodes.XTNode('00173b0013809448', conn)
#node2 = ar_nodes.XTNode('00173b001380ccdd', conn)
#node3 = ar_nodes.XTNode('00173b00138093aa', conn)


#dp_node1 = ar_nodes.DPNode('00173b001380cc1c', conn)
#print type(dp_node1.get_pressures())
#dp_node1.update()
#node = ar_nodes.XTNode('00173b001380a6d6')

#print node3.get_HTconfig_string()
#print node3.get_XTconfig_string()
#node3.update_config(20)
#print node3.get_HTconfig_string()
#print node3.get_XTconfig_string()

#print node3.send_config(20).toxml()

#print node1.get_humidity()
#print node1.get_temperatures()

ipsens1 = ar_nodes.IPSensor_RSN3010_2ADC('00173b0011698cda', conn)
#ipsens1._parse_temperature_event(ipsens1._request_temperature_event())
#print ipsens1.get_internal_temperature()
#ipsens1.update_internal_temperature()
#print ipsens1.get_internal_temperature()
#
#print ipsens1.get_internal_humidity()
#ipsens1.update_internal_humidity()
#print ipsens1.get_internal_humidity()
#
#print ipsens1.get_light_tsr()
#ipsens1.update_light_tsr()
#print ipsens1.get_light_tsr()
#
#print ipsens1.get_light_par()
#ipsens1.update_light_par()
##print ipsens1.get_light_par()
#print "ena" + str(ipsens1.is_switch_enabled())
#print "sam" + str(ipsens1.is_switch_sampling())
#print "int" + str(ipsens1.is_switch_interrupting())
#print ipsens1.update_switch_config()
#print "ena" + str(ipsens1.is_switch_enabled())
#print "sam" + str(ipsens1.is_switch_sampling())
#print "int" + str(ipsens1.is_switch_interrupting())
#
#ipsens1._switch_interrupting = True
#ipsens1.send_switch_config()
#ipsens1.update_switch_config()
#print "int " + str (ipsens1.is_switch_interrupting())

ippwr = ar_nodes.IPPower("00173b001380a79c", conn)

#ippwr.update_frequency_peaks(40)
#ippwr.update_voltages_peaks(40)
#ippwr.update_real_power_peaks(40)
#ippwr.update_reactive_power_peaks(40)
ippwr.update_apparent_power_peaks(40)


print ippwr.get_max_line_frequency()
print ippwr.get_min_line_frequency()
print ippwr.get_max_line_frequency_time()
print ippwr.get_min_line_frequency_time()
print ""

print ippwr.get_max_voltages()
print ippwr.get_min_voltages()
print ippwr.get_max_voltages_times()
print ippwr.get_min_voltages_times()

print ippwr.get_max_delivered_real_power()
print ippwr.get_max_received_real_power()
print ippwr.get_max_delivered_real_power_times()
print ippwr.get_max_received_real_power_times()

print ippwr.get_max_lead_power()
print ippwr.get_max_lag_power()
print ippwr.get_max_lead_power_times()
print ippwr.get_max_lag_power_times()

print ippwr.get_max_apparent_powers()
print ippwr.get_max_apparent_powers_times()
print ippwr.get_max_currents()
print ippwr.get_max_currents_times()

dp1 = ar_nodes.IPThermal_RSN3040_3DP("00173b0013805b44", conn)
dp1.blink()
print dp1.ping()

#ipsens1.get_port_state("Ports.SWITCH")
ipsens1.update_port_config("IO2")
ipsens1.enable_port_as_output("IO2", True)
print ipsens1.is_port_used_as_output("IO2")

print "sw"
ipsens1.update_port_config("Switch")
print ipsens1.is_port_enabled("Switch")
ipsens1.enable_port("Switch", not True)
ipsens1.send_port_config("Switch")
print ipsens1.is_port_enabled("Switch")

ippwr.update_config(30)
print ippwr.get_current_transformer_amps()
print ippwr.get_nominal_volts()

#s_detector = butt_detector.Switch_Detector()
#s_detector.add_sensor(ipsens1)
#s_detector.detect(2)

#detector = butt_detector.Temperature_Detector()
#detector.add_sensor(node1)
#detector.add_sensor(node2)
#detector.add_sensor(node3)
#detector.detect(1)
