'''
Created on 2 Aug 2012

@author: julian
'''

import urllib2
from xml.dom.minidom import parseString

class Conn:
    '''
    Provides a connection to a web service using HTTP Basic Auth
    and allows to send GET requests 
    '''
    _username = None
    _password = None
    _baseurl = None
    _timeout = None

    def __init__(self, username, password, baseurl, timeout=10):
        self._username = username
        self._password = password  
        self._baseurl = baseurl
        self._timeout = timeout
        
        passmgr = urllib2.HTTPPasswordMgrWithDefaultRealm()
        passmgr.add_password(None, self._baseurl, self.get_username(), self.get_password())
        authhandler = urllib2.HTTPBasicAuthHandler(passmgr)
        opener = urllib2.build_opener(authhandler)
        urllib2.install_opener(opener)
        
    def get_username(self):
        return self._username
    
    def set_username(self, username):
        self._username = username
    
    def get_password(self):
        return self._password
    
    def set_password(self, password):
        self._password = password
    
    def get_baseurl(self):
        return self._baseurl
    
    def set_baseurl(self, baseurl):
        self._baseurl = baseurl
    
    def get_timeout(self):
        return self._timeout
    
    def set_timeout(self, timeout):
        self._timeout = timeout
    
    def REST_GET(self, param):
        url = self._baseurl + param 
        response = urllib2.urlopen(url).read()
        dom = parseString(response)
        return dom
    
