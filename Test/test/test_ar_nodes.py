'''
Created on 8 Aug 2012

@author: julian
'''
import unittest
from xml.dom.minidom import parse

import ar_nodes
import connection

class TestNode(unittest.TestCase):
    '''
    Tests the connectivity. Quite slow, crashes when network is unavailable,
    possibly makes lots of redundant connections an is wrong in many other ways
    Use if you have to, it runs on production
    '''

    def setUp(self):
        self.conn = connection.Conn("guest", "veryTOPsecret", 
                                    "http://areo.cs.ucl.ac.uk/gw/rest/V1?")
        self.node = ar_nodes.Node("00173b00138093aa", self.conn)

    def test_address(self):
        self.assertEqual(self.node.get_address(), "00173b00138093aa")

    def test_address_change(self):
        self.node.set_address("00173b00138093ab")
        self.assertEqual(self.node.get_address(), "00173b00138093ab")

    def test_connection_change(self):
        temp_conn = self.node.get_connection()
        self.node.set_connection(None)
        self.assertEqual(self.node.get_connection(), None)
        self.node.set_connection(temp_conn)
        self.assertEqual(self.node.get_connection(), temp_conn)

    def test_sample_period_change(self):
        self.assertEqual(type(self.node.get_sample_period()), int)

        period = self.node.get_sample_period()
        self.assertEqual(type(period), int)

        self.node.set_sample_period(500000, 40)
        self.assertEqual(self.node.get_sample_period(), 500000)

        self.node.set_sample_period(period, 40)
        self.assertEqual(self.node.get_sample_period(), period)

    def test_blinking(self):
        self.node.blink()

    def test_pinging(self):
        self.assertTrue(self.node.ping())


class TestHTChannels(unittest.TestCase):
    '''
    Uses local files
    '''

    def setUp(self):
        self.ht = ar_nodes.HTChannels()

    def test_accessors(self):
        self.assertEqual(self.ht.get_internal_temperature(), None)
        self.assertEqual(self.ht.get_internal_humidity(), None)
        self.assertEqual(self.ht.is_internal_temperature_enabled(), None)
        self.assertEqual(self.ht.is_internal_humidity_enabled(), None)

        self.ht._set_internal_temperature(222)
        self.ht._set_internal_humidity(444)
        self.assertEqual(self.ht.get_internal_temperature(), 222)
        self.assertEqual(self.ht.get_internal_humidity(), 444)

        self.ht.enable_internal_temperature(True)
        self.ht.enable_internal_humidity(True)
        self.assertEqual(self.ht.is_internal_temperature_enabled(), True)
        self.assertEqual(self.ht.is_internal_humidity_enabled(), True)

        self.ht.enable_internal_temperature(False)
        self.ht.enable_internal_humidity(False)
        self.assertEqual(self.ht.is_internal_temperature_enabled(), False)
        self.assertEqual(self.ht.is_internal_humidity_enabled(), False)

    def test_event_reading(self):
        dom = parse("xml/RSN30406XTReadEvent_good.xml")
        self.ht.update_HTChannels(dom)
        self.assertEqual(self.ht.get_internal_temperature(), 320)
        self.assertEqual(self.ht.get_internal_humidity(), 265)

    def test_config_reading(self):
        self.ht.enable_internal_temperature(False)
        self.ht.enable_internal_humidity(False)
        self.assertEqual(self.ht.is_internal_temperature_enabled(), False)
        self.assertEqual(self.ht.is_internal_humidity_enabled(), False)

        dom = parse("xml/RSN30406XTConfig_good.xml")
        self.ht.update_HTconfig(dom)
        self.assertEqual(self.ht.is_internal_temperature_enabled(), True)
        self.assertEqual(self.ht.is_internal_humidity_enabled(), True)


class TestXTChannels(unittest.TestCase):
    '''
    Uses local files
    '''

    def setUp(self):
        self.xt = ar_nodes.XTChannels()

    def test_accessors(self):
        self.assertEqual(self.xt.get_temperatures(), [])

        self.xt._set_temperatures([1, 2, 3, 4, 5, 6])
        self.assertEqual(self.xt.get_temperatures(), [1, 2, 3, 4, 5, 6])

        self.xt._set_temperatures([1, 2, 3, 4, 5])
        self.assertEqual(self.xt.get_temperatures(), [])

        self.xt._set_temperatures([1, 2, 3, 4, 5, 6, 7])
        self.assertEqual(self.xt.get_temperatures(), [])

        self.assertEqual(self.xt.are_temperatures_enabled(), [])
        self.xt.enable_temperatures([True, True, True, False, False, False])
        self.assertEqual(self.xt.are_temperatures_enabled(), [True, True, True,
                                                            False, False, False])

        self.assertEqual(self.xt.get_temperatures_R0(), [])
        self.xt.set_temperatures_R0([1, 2, 3, 4, 5, 6])
        self.assertEqual(self.xt.get_temperatures_R0(), [1, 2, 3, 4, 5, 6])

        self.assertEqual(self.xt.get_temperatures_B(), [])
        self.xt.set_temperatures_B([2, 3, 4, 5, 6, 7])
        self.assertEqual(self.xt.get_temperatures_B(), [2, 3, 4, 5, 6, 7])

    def test_config_reading(self):
        self.assertEqual(self.xt.are_temperatures_enabled(), [])
        self.assertEqual(self.xt.get_temperatures_R0(), [])
        self.assertEqual(self.xt.get_temperatures_B(), [])

        dom = parse("xml/RSN30406XTConfig_good.xml")
        self.xt.update_XTconfig(dom)
        self.assertEqual(self.xt.are_temperatures_enabled(), [True, True, True,
                                                      True, False, False])
        self.assertEqual(self.xt.get_temperatures_R0(), [3000.0, 3000.0, 3000.0,
                                                 3000.0, 3000.0, 0])
        self.assertEqual(self.xt.get_temperatures_B(), [3890.0, 3890.0, 3890.0,
                                                3890.0, 3890.0, 1000])

    def test_event_reading(self):
        self.assertEqual(self.xt.get_temperatures(), [])
        dom = parse("xml/RSN30406XTReadEvent_good.xml")
        self.xt.update_XTChannels(dom)
        self.assertEqual(self.xt.get_temperatures(), [295, 195, 179,
                                                      170, None, None])


class TestDPChannels(unittest.TestCase):
    '''
    Uses local files
    '''

    def setUp(self):
        self.dp = ar_nodes.DPChannels()

    def test_accessors(self):
        self.assertEqual(self.dp.get_pressures(), [])
        self.assertEqual(self.dp.are_pressures_enabled(), [])
        self.assertEqual(self.dp.get_ambient_pressures(), [])

        self.dp._set_pressures([1, 2, 3])
        self.assertEqual(self.dp.get_pressures(), [1, 2, 3])

        self.dp.enable_pressures([True, False, True])
        self.assertEqual(self.dp.are_pressures_enabled(), [True, False, True])

        self.dp.set_ambient_pressures([2, 3, 4])
        self.assertEqual(self.dp.get_ambient_pressures(), [2, 3, 4])

    def test_event_reading(self):
        dom = parse("xml/RSN30403DPReadEvent_good.xml")
        self.dp.update_DPChannels(dom)
        self.assertEqual(self.dp.get_pressures(), [2166, 4116, None])

    def test_config_reading(self):
        dom = parse("xml/RSN30403DPConfig_good.xml")
        self.dp.update_DPconfig(dom)
        self.assertEqual(self.dp.are_pressures_enabled(), [True, True, False])
        self.assertEqual(self.dp.get_ambient_pressures(), [1005, 1005, 1005])


class TestIPThermal_RSN3040_6XT(unittest.TestCase):
    '''
    Tests node communication.
    Use if you have to, it runs on production
    '''

    def setUp(self):
        self.conn = connection.Conn("guest", "veryTOPsecret",
                                    "http://areo.cs.ucl.ac.uk/gw/rest/V1?")
        self.xtnode = ar_nodes.IPThermal_RSN3040_6XT("00173b00138093aa",
                                                     self.conn)

    def tearDown(self):
        pass

    def test_startup_state(self):
        self.assertEqual(self.xtnode.get_address(), "00173b00138093aa")
        self.assertEqual(self.xtnode.get_internal_temperature(), None)
        self.assertEqual(self.xtnode.get_internal_humidity(), None)
        self.assertEqual(self.xtnode.are_temperatures_enabled(), [])
        self.assertEqual(type(self.xtnode.get_sample_period()), int)
        self.assertEqual(self.xtnode.get_temperatures(), [])
        self.assertEqual(self.xtnode.get_temperatures_R0(), [])
        self.assertEqual(self.xtnode.get_temperatures_B(), [])

    def test_event_processing(self):
        self.xtnode.update_data(5)
        temps = self.xtnode.get_temperatures()
        for temp in temps:
            self.assertTrue(((temp < 500) and (temp > -32768))
                            or (temp == None))

    def test_expensive_event_processing(self):
        self.xtnode.update_data_expensive(40)
        temps = self.xtnode.get_temperatures()
        for temp in temps:
            self.assertTrue(((temp < 500) and (temp > -32768))
                            or (temp == None))

    def test_config_processing(self):
        self.xtnode.update_config(40)
        enabled = self.xtnode.are_temperatures_enabled()
        R0 = self.xtnode.get_temperatures_R0()
        B = self.xtnode.get_temperatures_B()

        for en in enabled:
            self.assertTrue(en)

        for r in R0:
            self.assertEqual(r, 3000.0)

        for b in B:
            self.assertEqual(b, 3890.0)

    def test_config_sending(self):
        self.xtnode.update_config(40)
        inttemp = self.xtnode.is_internal_temperature_enabled()
        inthum = self.xtnode.is_internal_humidity_enabled()
        xtenabled = self.xtnode.are_temperatures_enabled()
        xtr = self.xtnode.get_temperatures_R0()
        xtb = self.xtnode.get_temperatures_B()

        dom = self.xtnode.send_config(40)
        self.xtnode.update_HTconfig(dom)
        self.xtnode.update_XTconfig(dom)

        self.assertEqual(inttemp, self.xtnode.is_internal_temperature_enabled())
        self.assertEqual(inthum, self.xtnode.is_internal_humidity_enabled())
        self.assertEqual(xtenabled, self.xtnode.are_temperatures_enabled())
        self.assertEqual(xtr, self.xtnode.get_temperatures_R0())
        self.assertEqual(xtb, self.xtnode.get_temperatures_B())


class TestIPThermal_RSN3040_3DP(unittest.TestCase):
    '''
    Tests connectivity, it is slow, runs on production,
    keep disabled if not needed
    '''

    def setUp(self):
        self.conn = connection.Conn("guest", "veryTOPsecret",
                                    "http://areo.cs.ucl.ac.uk/gw/rest/V1?")
        self.dpnode = ar_nodes.IPThermal_RSN3040_3DP("00173b001380cc1c",
                                                     self.conn)

    def test_startup_state(self):
        self.assertEqual(self.dpnode.get_address(), "00173b001380cc1c")
        self.assertEqual(self.dpnode.get_pressures(), [])
        self.assertEqual(self.dpnode.get_ambient_pressures(), [])
        self.assertEqual(self.dpnode.get_internal_temperature(), None)
        self.assertEqual(self.dpnode.get_internal_temperature(), None)
        self.assertEqual(self.dpnode.get_internal_humidity(), None)
        self.assertEqual(self.dpnode.are_pressures_enabled(), [])

    def test_event_processing(self):
        self.dpnode.update_data(5)
        press = self.dpnode.get_pressures()
        self.assertEqual(len(press), 3)
        for pre in press:
            self.assertTrue((type(pre) == int) or (pre == None))

    def test_expensive_event_processing(self):
        self.dpnode.update_data_expensive(40)
        press = self.dpnode.get_pressures()
        self.assertEqual(len(press), 3)
        for pre in press:
            self.assertTrue((type(pre) == int) or (pre == None))

    def test_config_processing(self):
        self.dpnode.update_config(40)
        enabled = self.dpnode.are_pressures_enabled()
        amb = self.dpnode.get_ambient_pressures()

        for en in enabled:
            self.assertEquals(type(en), bool)

        for am in amb:
            self.assertEqual(am, 1005)

    def test_config_sending(self):
        self.dpnode.update_config()
        internal_temp = self.dpnode.is_internal_temperature_enabled()
        internal_hum = self.dpnode.is_internal_humidity_enabled()
        press_enabled = self.dpnode.are_pressures_enabled()

        self.dpnode.send_config()

        self.assertEquals(internal_temp,
                          self.dpnode.is_internal_temperature_enabled())
        self.assertEquals(internal_hum,
                          self.dpnode.is_internal_humidity_enabled())
        self.assertEquals(press_enabled, self.dpnode.are_pressures_enabled())


class TestIPSensor_2ADC(unittest.TestCase):
    def setUp(self):
        self.conn = connection.Conn("guest", "veryTOPsecret",
                                    "http://areo.cs.ucl.ac.uk/gw/rest/V1?")
        self.ipsens = ar_nodes.IPSensor_RSN3010_2ADC("00173b0011698cda", self.conn)

    def test_startup_state(self):
        self.assertEquals(self.ipsens.get_address(), "00173b0011698cda")
        self.assertEquals(self.ipsens.get_internal_temperature(), None)
        self.assertEquals(self.ipsens.is_internal_temperature_enabled(), None)
        self.assertEquals(self.ipsens.get_internal_humidity(), None)
        self.assertEquals(self.ipsens.is_internal_humidity_enabled(), None)
        self.assertEquals(self.ipsens.get_light_tsr(), None)
        self.assertEquals(self.ipsens.is_light_tsr_enabled(), None)
        self.assertEquals(self.ipsens.get_light_par(), None)
        self.assertEquals(self.ipsens.is_light_par_enabled(), None)
        self.assertEquals(self.ipsens.get_port_state("Switch"), None)
        self.assertEquals(self.ipsens.is_port_enabled("Switch"), None)
        self.assertEquals(self.ipsens.is_port_interrupting("Switch"), None)
        self.assertEquals(self.ipsens.is_port_sampling("Switch"), None)

    def test_updating_internal_temp(self):
        self.ipsens.update_internal_temperature(5)
        temp = self.ipsens.get_internal_temperature()
        self.assertEquals(type(temp), int)
        self.assertGreater(temp, 100)
        self.assertLess(temp, 400)

    def test_updating_internal_temp_expensive(self):
        self.ipsens.update_internal_temperature_expensive(40)
        temp = self.ipsens.get_internal_temperature()
        self.assertEquals(type(temp), int)
        self.assertGreater(temp, 100)
        self.assertLess(temp, 400)

    def test_updating_internal_hum(self):
        self.ipsens.update_internal_humidity(5)
        hum = self.ipsens.get_internal_humidity()
        self.assertEquals(type(hum), int)
        self.assertGreater(hum, 300)
        self.assertLess(hum, 700)

    def test_updating_internal_hum_expensive(self):
        self.ipsens.update_internal_humidity_expensive(40)
        hum = self.ipsens.get_internal_humidity()
        self.assertEquals(type(hum), int)
        self.assertGreater(hum, 300)
        self.assertLess(hum, 700)

    def test_updating_light_tsr(self):
        self.ipsens.update_light_tsr(5)
        tsr = self.ipsens.get_light_tsr()
        self.assertEquals(type(tsr), int)
        self.assertGreater(tsr, 0)
        self.assertLess(tsr, 100)

    def test_updating_light_tsr_expensive(self):
        self.ipsens.update_light_tsr_expensive(40)
        tsr = self.ipsens.get_light_tsr()
        self.assertEquals(type(tsr), int)
        self.assertGreater(tsr, 0)
        self.assertLess(tsr, 100)

    def test_updating_light_par(self):
        self.ipsens.update_light_par(5)
        par = self.ipsens.get_light_par()
        self.assertEquals(type(par), int)
        self.assertGreater(par, 10)
        self.assertLess(par, 1000)

    def test_updating_light_par_expensive(self):
        self.ipsens.update_light_par_expensive(40)
        par = self.ipsens.get_light_par()
        self.assertEquals(type(par), int)
        self.assertGreater(par, 10)
        self.assertLess(par, 1000)

    def test_updating_switch(self):
        self.ipsens.update_port("Switch", 5)
        switch = self.ipsens.get_port_state("Switch")
        self.assertTrue((switch == 0) or (switch == 1))

#    def test_updating_switch_expensive(self):
#        self.ipsens.update_port_expensive("Switch")
#        switch = self.ipsens.get_port_state("Switch")
#        self.assertTrue((switch == 0) or (switch == 1))

    def test_config_processing(self):
        self.ipsens.update_port_config("Switch", 40)

        enabled = False
        if self.ipsens.is_port_enabled("Switch"):
            enabled = True

        sampling = False
        if self.ipsens.is_port_sampling("Switch"):
            sampling = True

        interrupting = False
        if self.ipsens.is_port_interrupting("Switch"):
            interrupting = True

        self.ipsens.enable_port("Switch", not enabled)
        self.ipsens.enable_port_sampling("Switch", not sampling)
        self.ipsens.enable_port_interrupting("Switch", not interrupting)

        self.ipsens.send_port_config("Switch")
        self.assertEquals(self.ipsens.is_port_enabled("Switch"), not enabled)
        self.assertEquals(self.ipsens.is_port_sampling("Switch"), not sampling)
        self.assertEquals(self.ipsens.is_port_interrupting("Switch"), not interrupting)

        self.ipsens.enable_port("Switch", enabled)
        self.ipsens.enable_port_sampling("Switch", sampling)
        self.ipsens.enable_port_interrupting("Switch", interrupting)
        self.ipsens.send_port_config("Switch")

    def test_changing_port_states(self):
        self.ipsens._set_port_state("Switch", 0)
        self.ipsens._set_port_state("IO0", 0)
        self.ipsens._set_port_state("IO1", 0)
        self.ipsens._set_port_state("IO2", 0)

        self.assertEquals(self.ipsens.get_port_state("Switch"), 0)
        self.assertEquals(self.ipsens.get_port_state("IO0"), 0)
        self.assertEquals(self.ipsens.get_port_state("IO1"), 0)
        self.assertEquals(self.ipsens.get_port_state("IO2"), 0)

    def test_port_enabling(self):
        self.ipsens.enable_port("Switch", True)
        self.ipsens.enable_port("IO0", True)
        self.ipsens.enable_port("IO1", True)
        self.ipsens.enable_port("IO2", True)

        self.assertTrue(self.ipsens.is_port_enabled("Switch"))
        self.assertTrue(self.ipsens.is_port_enabled("IO0"))
        self.assertTrue(self.ipsens.is_port_enabled("IO1"))
        self.assertTrue(self.ipsens.is_port_enabled("IO2"))

    def test_port_sampling_enabling(self):
        self.ipsens.enable_port_sampling("Switch", True)
        self.ipsens.enable_port_sampling("IO0", True)
        self.ipsens.enable_port_sampling("IO1", True)
        self.ipsens.enable_port_sampling("IO2", True)

        self.assertTrue(self.ipsens.is_port_sampling("Switch"))
        self.assertTrue(self.ipsens.is_port_sampling("IO0"))
        self.assertTrue(self.ipsens.is_port_sampling("IO1"))
        self.assertTrue(self.ipsens.is_port_sampling("IO2"))

    def test_port_interupting_enabling(self):
        self.ipsens.enable_port_interrupting("Switch", False)
        self.ipsens.enable_port_interrupting("IO0", False)
        self.ipsens.enable_port_interrupting("IO1", False)
        self.ipsens.enable_port_interrupting("IO2", False)

        self.assertFalse(self.ipsens.is_port_interrupting("Switch"))
        self.assertFalse(self.ipsens.is_port_interrupting("IO0"))
        self.assertFalse(self.ipsens.is_port_interrupting("IO1"))
        self.assertFalse(self.ipsens.is_port_interrupting("IO2"))

    def test_port_as_output(self):
        self.ipsens.enable_port_as_output("Switch", False)
        self.ipsens.enable_port_as_output("IO0", False)
        self.ipsens.enable_port_as_output("IO1", False)
        self.ipsens.enable_port_as_output("IO2", False)

        self.assertFalse(self.ipsens.is_port_used_as_output("Switch"))
        self.assertFalse(self.ipsens.is_port_used_as_output("IO0"))
        self.assertFalse(self.ipsens.is_port_used_as_output("IO1"))
        self.assertFalse(self.ipsens.is_port_used_as_output("IO2"))


class TestIPPower(unittest.TestCase):
    def setUp(self):
        self.conn = connection.Conn("guest", "veryTOPsecret",
                                    "http://areo.cs.ucl.ac.uk/gw/rest/V1?")
        self.ippwr = ar_nodes.IPPower("00173b001380a79c", self.conn)

    def test_startup_state(self):
        self.assertEquals(self.ippwr.are_phases_enabled(), [True, True, True])
        self.assertEquals(self.ippwr.get_current_transformer_amps(), [])
        self.assertEquals(self.ippwr.get_nominal_volts(), [])

        self.assertEquals(self.ippwr.get_voltages(), [])
        self.assertEquals(self.ippwr.get_currents(), [])
        self.assertEquals(self.ippwr.get_real_powers(), [])
        self.assertEquals(self.ippwr.get_reactive_powers(), [])
        self.assertEquals(self.ippwr.get_apparent_powers(), [])
        self.assertEquals(self.ippwr.get_line_frequency(), None)

        self.assertEquals(self.ippwr.get_max_line_frequency(), None)
        self.assertEquals(self.ippwr.get_min_line_frequency(), None)
        self.assertEquals(self.ippwr.get_max_line_frequency_time(), None)
        self.assertEquals(self.ippwr.get_min_line_frequency_time(), None)

        self.assertEquals(self.ippwr.get_max_voltages(), [])
        self.assertEquals(self.ippwr.get_min_voltages(), [])
        self.assertEquals(self.ippwr.get_max_voltages_times(), [])
        self.assertEquals(self.ippwr.get_min_voltages_times(), [])

        self.assertEquals(self.ippwr.get_max_delivered_real_power(), [])
        self.assertEquals(self.ippwr.get_max_received_real_power(), [])
        self.assertEquals(self.ippwr.get_max_delivered_real_power_times(), [])
        self.assertEquals(self.ippwr.get_max_received_real_power_times(), [])

        self.assertEquals(self.ippwr.get_max_lead_power(), [])
        self.assertEquals(self.ippwr.get_max_lead_power_times(), [])
        self.assertEquals(self.ippwr.get_max_lag_power(), [])
        self.assertEquals(self.ippwr.get_max_lag_power_times(), [])

        self.assertEquals(self.ippwr.get_max_apparent_powers(), [])
        self.assertEquals(self.ippwr.get_max_apparent_powers_times(), [])
        self.assertEquals(self.ippwr.get_max_currents(), [])
        self.assertEquals(self.ippwr.get_max_currents_times(), [])

    def test_frequency_accessors(self):
        self.ippwr._set_max_line_frequency(300)
        self.ippwr._set_max_line_frequency_time(3000)
        self.ippwr._set_min_line_frequency(200)
        self.ippwr._set_min_line_frequency_time(2000)

        self.assertEquals(self.ippwr.get_max_line_frequency(), 300)
        self.assertEquals(self.ippwr.get_max_line_frequency_time(), 3000)
        self.assertEquals(self.ippwr.get_min_line_frequency(), 200)
        self.assertEquals(self.ippwr.get_min_line_frequency_time(), 2000)

    def test_voltage_peaks_accessors(self):
        self.ippwr._set_max_voltages([1, 2, 3])
        self.ippwr._set_max_voltages_times([111, 222, 333])
        self.ippwr._set_min_voltages([4, 5, 6])
        self.ippwr._set_min_voltages_times([444, 555, 666])

        self.assertEquals(self.ippwr.get_max_voltages(), [1, 2, 3])
        self.assertEquals(self.ippwr.get_max_voltages_times(), [111, 222, 333])
        self.assertEquals(self.ippwr.get_min_voltages(), [4, 5, 6])
        self.assertEquals(self.ippwr.get_min_voltages_times(), [444, 555, 666])

    def test_real_power_peaks_accessors(self):
        self.ippwr._set_max_delivered_real_power([1, 2, 3])
        self.ippwr._set_max_received_real_power([4, 5, 6])
        self.ippwr._set_max_delivered_real_power_times([11, 22, 33])
        self.ippwr._set_max_received_real_power_times([44, 55, 66])

        self.assertEquals(self.ippwr.get_max_delivered_real_power(),
                          [1, 2, 3])
        self.assertEquals(self.ippwr.get_max_received_real_power(),
                          [4, 5, 6])
        self.assertEquals(self.ippwr.get_max_delivered_real_power_times(),
                          [11, 22, 33])
        self.assertEquals(self.ippwr.get_max_received_real_power_times(),
                          [44, 55, 66])

    def test_reactive_power_peaks_accessors(self):
        self.ippwr._set_max_lead_power([1, 2, 3])
        self.ippwr._set_max_lag_power([4, 5, 6])
        self.ippwr._set_max_lead_power_times([11, 22, 33])
        self.ippwr._set_max_lag_power_times([44, 55, 66])

        self.assertEquals(self.ippwr.get_max_lead_power(), [1, 2, 3])
        self.assertEquals(self.ippwr.get_max_lag_power(), [4, 5, 6])
        self.assertEquals(self.ippwr.get_max_lead_power_times(), [11, 22, 33])
        self.assertEquals(self.ippwr.get_max_lag_power_times(), [44, 55, 66])


    def test_apparent_power_peaks_accessots(self):
        self.ippwr._set_max_apparent_powers([1, 2, 3])
        self.ippwr._set_max_apparent_powers_times([11, 22, 33])
        self.ippwr._set_max_currents([4, 5, 6])
        self.ippwr._set_max_currents_times([44, 55, 66])

        self.assertEquals(self.ippwr.get_max_apparent_powers(), [1, 2, 3])
        self.assertEquals(self.ippwr.get_max_apparent_powers_times(), [11, 22, 33])
        self.assertEquals(self.ippwr.get_max_currents(), [4, 5, 6])
        self.assertEquals(self.ippwr.get_max_currents_times(), [44, 55, 66])

    def test_config_processing(self):
        self.ippwr.update_config(40)
        self.assertEquals(self.ippwr.are_phases_enabled(), [True, True, True])

        CTs = []
        volts = []

        for ct in self.ippwr.get_current_transformer_amps():
            self.assertGreater(ct, 29)
            self.assertLess(ct, 1501)
            CTs.append(ct)

        for v in self.ippwr.get_nominal_volts():
            self.assertGreater(v, 100)
            self.assertLess(v, 500)
            volts.append(v)

        self.ippwr.send_config(40)
        self.ippwr.update_config(40)

        self.assertEquals(CTs, self.ippwr.get_current_transformer_amps())
        self.assertEquals(volts, self.ippwr.get_nominal_volts())

    def test_MultiElectricalDemand_processing(self):
        self.ippwr.update_data(40)

        for v in self.ippwr.get_voltages():
            self.assertEquals(type(v), int)

        for a in self.ippwr.get_currents():
            self.assertEquals(type(a), int)

        for w in self.ippwr.get_real_powers():
            self.assertEquals(type(w), int)

        for r in self.ippwr.get_reactive_powers():
            self.assertEquals(type(r), int)

        for app in self.ippwr.get_apparent_powers():
            self.assertEquals(type(app), int)

        self.assertEquals(type(self.ippwr.get_line_frequency()), int)

    def test_MultiElectricalDemand_processing_expensive(self):
        self.ippwr.update_data_expensive(40)

        for v in self.ippwr.get_voltages():
            self.assertEquals(type(v), int)

        for a in self.ippwr.get_currents():
            self.assertEquals(type(a), int)

        for w in self.ippwr.get_real_powers():
            self.assertEquals(type(w), int)

        for r in self.ippwr.get_reactive_powers():
            self.assertEquals(type(r), int)

        for app in self.ippwr.get_apparent_powers():
            self.assertEquals(type(app), int)

        self.assertEquals(type(self.ippwr.get_line_frequency()), int)

    def test_frequency_peaks_processing(self):
        self.ippwr.update_frequency_peaks(40)

        max_freq = self.ippwr.get_max_line_frequency()
        min_freq = self.ippwr.get_min_line_frequency()
        max_time = self.ippwr.get_max_line_frequency_time()
        min_time = self.ippwr.get_min_line_frequency_time()

        self.assertGreater(max_freq, 400)
        self.assertLess(max_freq, 600)
        self.assertGreater(min_freq, 400)
        self.assertLess(min_freq, 600)
        self.assertGreater(max_freq, min_freq)

        self.assertGreater(max_time, 1340000000)
        self.assertGreater(min_time, 1340000000)

    def test_frequency_peaks_processing_expensive(self):
        self.ippwr.update_frequency_peaks_expensive(40)

        max_freq = self.ippwr.get_max_line_frequency()
        min_freq = self.ippwr.get_min_line_frequency()
        max_time = self.ippwr.get_max_line_frequency_time()
        min_time = self.ippwr.get_min_line_frequency_time()

        self.assertGreater(max_freq, 400)
        self.assertLess(max_freq, 600)
        self.assertGreater(min_freq, 400)
        self.assertLess(min_freq, 600)
        self.assertGreater(max_freq, min_freq)

        self.assertGreater(max_time, 1340000000)
        self.assertGreater(min_time, 1340000000)

    def test_voltages_peaks_processing(self):
        self.ippwr.update_voltages_peaks(40)

        max_volts = self.ippwr.get_max_voltages()
        for v in max_volts:
            self.assertEquals(type(v), int)

        min_volts = self.ippwr.get_min_voltages()
        for v in min_volts:
            self.assertEquals(type(v), int)

        for v in self.ippwr.get_max_voltages_times():
            self.assertEquals(type(v), int)

        for v in self.ippwr.get_min_voltages_times():
            self.assertEquals(type(v), int)

        for i in range(len(max_volts)):
            self.assertGreaterEqual(max_volts[i], min_volts[i])

        self.assertGreater(max_volts[0], 2000)
        self.assertLess(max_volts[0], 4000)
        self.assertGreater(min_volts[0], 2000)
        self.assertLess(min_volts[0], 4000)

    def test_voltages_peaks_processing_expensive(self):
        self.ippwr.update_voltages_peaks_expensive(40)

        max_volts = self.ippwr.get_max_voltages()
        for v in max_volts:
            self.assertEquals(type(v), int)

        min_volts = self.ippwr.get_min_voltages()
        for v in min_volts:
            self.assertEquals(type(v), int)

        for v in self.ippwr.get_max_voltages_times():
            self.assertEquals(type(v), int)

        for v in self.ippwr.get_min_voltages_times():
            self.assertEquals(type(v), int)

        for i in range(len(max_volts)):
            self.assertGreaterEqual(max_volts[i], min_volts[i])

        self.assertGreater(max_volts[0], 2000)
        self.assertLess(max_volts[0], 4000)
        self.assertGreater(min_volts[0], 2000)
        self.assertLess(min_volts[0], 4000)

    def test_max_real_power_processing(self):
        self.ippwr.update_real_power_peaks(40)

        max_dlvd = self.ippwr.get_max_delivered_real_power()
        max_rcvd = self.ippwr.get_max_received_real_power()
        max_d_t = self.ippwr.get_max_delivered_real_power_times()
        max_r_t = self.ippwr.get_max_received_real_power_times()

        for r in max_dlvd:
            self.assertEquals(type(r), int)
        for r in max_rcvd:
            self.assertEquals(type(r), int)
        for r in max_d_t:
            self.assertEquals(type(r), int)
        for r in max_r_t:
            self.assertEquals(type(r), int)

    def test_max_real_power_processing_expensive(self):
        self.ippwr.update_real_power_peaks_expensive(40)

        max_dlvd = self.ippwr.get_max_delivered_real_power()
        max_rcvd = self.ippwr.get_max_received_real_power()
        max_d_t = self.ippwr.get_max_delivered_real_power_times()
        max_r_t = self.ippwr.get_max_received_real_power_times()

        for r in max_dlvd:
            self.assertEquals(type(r), int)
        for r in max_rcvd:
            self.assertEquals(type(r), int)
        for r in max_d_t:
            self.assertEquals(type(r), int)
        for r in max_r_t:
            self.assertEquals(type(r), int)

    def test_reactive_power_peaks_processing(self):
        self.ippwr.update_reactive_power_peaks(40)

        leads = self.ippwr.get_max_lead_power()
        lags = self.ippwr.get_max_lag_power()
        leads_t = self.ippwr.get_max_lead_power_times()
        lags_t = self.ippwr.get_max_lag_power_times()

        for r in leads:
            self.assertEquals(type(r), int)
        for r in lags:
            self.assertEquals(type(r), int)
        for r in leads_t:
            self.assertEquals(type(r), int)
        for r in lags_t:
            self.assertEquals(type(r), int)

    def test_reactive_power_peaks_processing_expensive(self):
        self.ippwr.update_reactive_power_peaks_expensive(40)

        leads = self.ippwr.get_max_lead_power()
        lags = self.ippwr.get_max_lag_power()
        leads_t = self.ippwr.get_max_lead_power_times()
        lags_t = self.ippwr.get_max_lag_power_times()

        for r in leads:
            self.assertEquals(type(r), int)
        for r in lags:
            self.assertEquals(type(r), int)
        for r in leads_t:
            self.assertEquals(type(r), int)
        for r in lags_t:
            self.assertEquals(type(r), int)

    def test_apparent_power_peaks_processing(self):
        self.ippwr.update_apparent_power_peaks(40)

        apps = self.ippwr.get_max_apparent_powers()
        currs = self.ippwr.get_max_currents()
        apps_t = self.ippwr.get_max_apparent_powers_times()
        currs_t = self.ippwr.get_max_currents_times()

        for r in apps:
            self.assertEquals(type(r), int)
        for r in currs:
            self.assertEquals(type(r), int)
        for r in apps_t:
            self.assertEquals(type(r), int)
        for r in currs_t:
            self.assertEquals(type(r), int)

    def test_apparent_power_peaks_processing_expensive(self):
        self.ippwr.update_apparent_power_peaks_expensive(40)

        apps = self.ippwr.get_max_apparent_powers()
        currs = self.ippwr.get_max_currents()
        apps_t = self.ippwr.get_max_apparent_powers_times()
        currs_t = self.ippwr.get_max_currents_times()

        for r in apps:
            self.assertEquals(type(r), int)
        for r in currs:
            self.assertEquals(type(r), int)
        for r in apps_t:
            self.assertEquals(type(r), int)
        for r in currs_t:
            self.assertEquals(type(r), int)

    def non_test_method(self):
        self.assertTrue(False)

#if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    #suiteXTChannels = unittest.makeSuite(XTChannels, 'test')
    #suiteHTChannels = unittest.makeSuite(HTChannels, 'test')

    #runner = unittest.TextTestRunner()
    #runner.run(suiteHTChannels)
    #unittest.main()
#    suite = unittest.TestLoader().loadTestsFromTestCase(TestXTChannels)
#suite = unittest.TestLoader().loadTestsFromTestCase(TestNode)
#runner = unittest.TextTestRunner()
#runner.run(suite)

#runner.run(suite)
#    unittest.TextTestRunner().run(suite)
